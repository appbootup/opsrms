package com.snapdeal.payments.roleManagement.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.services.impl.UpdateUserServiceImpl;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.UpdateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.User;

public class UpdateUserServiceImplTest {
	
	@InjectMocks
	private UpdateUserServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<UpdateUserRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	private Boolean isUserExist(String userId){
		User user = new User();
		user.setId("1234");
		if(userId.equals(user.getId())){
			 return true; 
		 }
		return false;
	  }
	@Test
	public void testUpdateUserSuccess() {
		UpdateUserRequest user = new UpdateUserRequest();
		user.setUserId("1234");
		user.setName("Lohia");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(isUserExist("1234"));
		service.updateUser(user);
	
	}
	@Test(expected=NoSuchUserException.class)
	public void testUpdateUserFailure() {
		UpdateUserRequest user = new UpdateUserRequest();
		user.setUserId("12345");
		user.setName("Lohia");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(isUserExist("12345"));
		service.updateUser(user);
	}
	
	@Test(expected=ValidationException.class)
	public void testUpdateUserValidationFailure(){
		UpdateUserRequest user = new UpdateUserRequest();
		service.updateUser(user);
	}
}
