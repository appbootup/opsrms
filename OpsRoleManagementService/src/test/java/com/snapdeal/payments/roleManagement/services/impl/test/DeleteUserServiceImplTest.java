package com.snapdeal.payments.roleManagement.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.services.impl.DeleteUserServiceImpl;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.DeleteUserRequest;

public class DeleteUserServiceImplTest {
	
	@InjectMocks
	private DeleteUserServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<DeleteUserRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	@Test
	public void testDeleteUserSuccess() {
		DeleteUserRequest request = new DeleteUserRequest();
		request.setUserId("randomId");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(true);
		service.deleteUser(request);
	
	}
	@Test(expected=NoSuchUserException.class)
	public void testCreateUserFailure() {
		DeleteUserRequest request = new DeleteUserRequest();
		request.setUserId("randomId");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(false);
		service.deleteUser(request);
	}
	
	@Test(expected=ValidationException.class)
	public void testCreateUserValidationFailure(){
		DeleteUserRequest user = new DeleteUserRequest();
		service.deleteUser(user);
	}
}
