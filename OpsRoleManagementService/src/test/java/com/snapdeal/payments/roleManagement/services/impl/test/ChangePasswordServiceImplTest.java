package com.snapdeal.payments.roleManagement.services.impl.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.handlers.TokenUtils;
import com.snapdeal.payments.roleManagement.services.impl.ChangePasswordServiceImpl;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.UnAuthrizedException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.ChangePasswordRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenUtils.class)
public class ChangePasswordServiceImplTest {

   @InjectMocks
   private ChangePasswordServiceImpl service;

   @Mock
   private PersistanceManager persistanceManager;
   
   @Mock
   private IDFactory idFactory;

   @Spy
   private RequestParamValidatorForRole<ChangePasswordRequest> requestValidator = new RequestParamValidatorForRole<>();

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);
   }
   
	private TokenDTO getUserByTokenId(String token) {
		if (token.equals("1234"))
			return new TokenDTO("dummy", new Date(), true, "dummy");
		else
			return new TokenDTO("dummy", new Date(), false, "dummy");
	}

   @Test
   public void testChangePasswordSuccess() {
      ChangePasswordRequest request = new ChangePasswordRequest();

      request.setRequestToken("1234");
      request.setToken("1234");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "1234");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
		Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new Date());
      service.changePassword(request);
   }
   
   @Test(expected=ValidationException.class)
   public void testChangePasswordInvalidTokenFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();

      request.setRequestToken("12345");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "12345");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
		Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new Date());
      service.changePassword(request);
   }

   @Test(expected = UnAuthrizedException.class)
   public void testChangePasswordFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();
      request.setRequestToken("1234");
      request.setToken("dummyToken");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
      Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "dummyUser");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(null);
      service.changePassword(request);
   }

   @Test(expected = ValidationException.class)
   public void testChangePasswordValidationFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();
      service.changePassword(request);
   }
}
