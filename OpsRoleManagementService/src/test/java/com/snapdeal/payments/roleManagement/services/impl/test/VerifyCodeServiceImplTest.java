package com.snapdeal.payments.roleManagement.services.impl.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.services.impl.VerifyCodeServiceImpl;
import com.snapdeal.payments.roleManagementModel.exceptions.InvalidCodeException;
import com.snapdeal.payments.roleManagementModel.request.VerifyCodeRequest;

public class VerifyCodeServiceImplTest {

	@InjectMocks
	private VerifyCodeServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<VerifyCodeRequest> requestValidator = new RequestParamValidatorForRole<>();
	
	@Mock
	private IDFactory idfactory;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testVerifyCodeSuccess() {
		VerifyCodeRequest request = new VerifyCodeRequest();
		request.setVerificationCode("GwdxqUu-QLCXmNRT9pwFH2gBRogyBDw4Azsg0cgXXjt9oZJUUQdUWcwczLpt0bgB");
		request.setPassword("dummyPassword");
		Mockito.when(persistanceManager.getUserFromCode(Mockito.any(String.class)))
				.thenReturn("dummyUserId");
		Mockito.doNothing().when(persistanceManager).changePasswordUsingId(Mockito.any(String.class),
				Mockito.any(String.class),Mockito.any(Date.class));
		service.verifyCodeAndSetPassword(request);
	}

	@Test(expected = InvalidCodeException.class)
	public void testVerifyCodeFailure() {
		VerifyCodeRequest request = new VerifyCodeRequest();
		request.setVerificationCode("shouldfailhere");
		request.setPassword("dummyPassword");
		Mockito.when(persistanceManager.getUserFromCode(Mockito.any(String.class)))
				.thenReturn("dummyUserId");
		Mockito.doNothing().when(persistanceManager).changePasswordUsingId(Mockito.any(String.class),
				Mockito.any(String.class),Mockito.any(Date.class));
		service.verifyCodeAndSetPassword(request);

	}
}
