package com.snapdeal.payments.roleManagement.services.impl.test;

import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.handlers.TokenUtils;
import com.snapdeal.payments.roleManagement.services.impl.LoginUserServiceImpl;
import com.snapdeal.payments.roleManagementModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.UnAuthrizedUserException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.User;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenUtils.class)
public class LoginUserServiceImplTest {

   @InjectMocks
   private LoginUserServiceImpl service;

   @Mock
   private PersistanceManager persistanceManager;
   
   @Mock
   private TokenUtils tokenUtils;

   @Spy
   private IDFactory idfactory = new IDFactory();

   @Spy
   private RequestParamValidatorForRole<LoginUserRequest> requestValidator = new RequestParamValidatorForRole<>();

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);

   }

   private User getUserByUserName(String userName) {
      User user = new User();
      user.setEmail("dummyEmail");
      user.setUserName(userName);
      return user;
   }

   @Test
   public void testLoginUserValidTokenSuccess() {
      LoginUserRequest request = new LoginUserRequest();
      request.setUserName("aniket.lohia");
      request.setPassword("aniketlohia");
      TokenDTO tokenDto = new TokenDTO("dummy", new Date(), true, "dummyUser");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new DateTime().plusDays(45).toDate());
      Mockito.when(persistanceManager.getTokenFromUsername(Mockito.any(String.class))).thenReturn(tokenDto);
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(
               getUserByUserName(request.getUserName()));
      service.loginUser(request);
   }
   
   @Test
   public void testLoginUserInvalidTokenSuccess() {
      LoginUserRequest request = new LoginUserRequest();
      request.setUserName("aniket.lohia");
      request.setPassword("aniketlohia");
      GenerateTokenResponse response = new GenerateTokenResponse("dummyId", "dummy");
      PowerMockito.mockStatic(TokenUtils.class);
      TokenDTO tokenDto = new TokenDTO("dummy", new Date(), false, "dummyUser");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new DateTime().plusDays(45).toDate());
      Mockito.when(tokenUtils.generateToken()).thenReturn(response);
      Mockito.when(persistanceManager.getTokenFromUsername(Mockito.any(String.class))).thenReturn(tokenDto);
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(
               getUserByUserName(request.getUserName()));
      service.loginUser(request);
   }
   
   @Test(expected= UnAuthrizedUserException.class)
   public void testLoginUserPasswordFailure() {
      LoginUserRequest request = new LoginUserRequest();
      request.setUserName("aniket.lohia");
      request.setPassword("aniketlohia");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new DateTime().toDate());
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(
               getUserByUserName(request.getUserName()));
      service.loginUser(request);
   }

   @Test(expected = UnAuthrizedUserException.class)
   public void testLoginUserFailure() {
      LoginUserRequest request = new LoginUserRequest();
      request.setUserName("aniket.lohia");
      request.setPassword("aniketlohia");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new DateTime().toDate());
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(
               getUserByUserName(request.getUserName()));
      service.loginUser(request);
   }

   @Test(expected = ValidationException.class)
   public void testLoginUserValidationFailure() {
      LoginUserRequest request = new LoginUserRequest();
      service.loginUser(request);
   }
}
