package com.snapdeal.payments.roleManagement.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.services.impl.GetUserByUserNameServiceImpl;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.request.User;

public class GetUserByEmailServiceImplTest {
	@InjectMocks
	private GetUserByUserNameServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetUserByUserNameRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private User getUserByUserName(String userName){
			User user = new User();
			user.setUserName("random123@gmail.com");
			user.setName("Aniket");
			return user;
		  }
		@Test
		public void testGetUsersByRoleSuccess() {
			GetUserByUserNameRequest request = new GetUserByUserNameRequest();
			request.setUserName("1234");
			Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class)))
			.thenReturn(getUserByUserName(request.getUserName()));
			service.getUserByUserName(request);
		}
		@Test(expected=ValidationException.class)
		public void testGetUsersByRoleValidationFailure(){
			GetUserByUserNameRequest request = new GetUserByUserNameRequest();
			service.getUserByUserName(request);
		}

}
