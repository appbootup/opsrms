-- Script for permissions of super user

insert into permissions(id,app_name,permission_name) values(1002,"ROLEMGMT","MCNT_VIEW_USER_PERM");
insert into permissions(id,app_name,permission_name) values(1003,"ROLEMGMT","MCNT_ADD_USER_PERM");
insert into permissions(id,app_name,permission_name) values(1004,"ROLEMGMT","MCNT_EDIT_USER_PERM");
insert into permissions(id,app_name,permission_name) values(1005,"ROLEMGMT","MCNT_VIEWALL_USER_PERM");
insert into permissions(id,app_name,permission_name) values(1006,"ROLEMGMT","FORGET_PWD_PERM");
insert into permissions(id,app_name,permission_name) values(1007,"ROLEMGMT","CHANGE_PWD_PERM");
insert into permissions(id,app_name,permission_name) values(1008,"ROLEMGMT","MCNT_PROFILE_VIEW_PERM");

insert into user_permission_mapping values(1002,1001);
insert into user_permission_mapping values(1003,1001);
insert into user_permission_mapping values(1004,1001);
insert into user_permission_mapping values(1005,1001);
insert into user_permission_mapping values(1006,1001);
insert into user_permission_mapping values(1007,1001);
insert into user_permission_mapping values(1008,1001);