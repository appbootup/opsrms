use role_mgmt;
alter table permissions drop index app_name;
alter table permissions add UNIQUE (permission_name);
update permissions set display_name="Manage user" where permission_name="MANAGE_USER";