DELETE FROM user_permission_mapping WHERE user_id=(SELECT id FROM user WHERE name='avinash.katiyar@snapdeal.com');

DELETE FROM user_permission_mapping WHERE user_id=(SELECT id FROM user WHERE name='saheb.singh@snapdeal.com');

DELETE FROM permissions where permission_name='actionpanel_bulkAction' or permission_name='actionpanel_singleAction' or permission_name='ops_superuser' or permission_name='OPS_INTERNALPANEL_superuser' or app_name='OPS_USERPANEL' or app_name='OPS_REPORTPANEL'or app_name='OPS_BULKPANEL';

INSERT INTO permissions(app_name,permission_name,display_name) VALUES 
('OPS_INTERNALPANEL','OPS_INTERNALPANEL_superuser','OPS SuperUser')

,('OPS_USERPANEL','actionpanel_singleAction','OPS Account Action')
,('OPS_USERPANEL','actionpanel_bulkAction','OPS Bulk Accounts Action')
,('OPS_USERPANEL','OPS_USERPANEL_superuser','OPS Action Panel SuperUser')
,('OPS_USERPANEL','OPS_USERPANEL_view','OPS Info Panel view permission')

,('OPS_REPORTPANEL','OPS_REPORTPANEL_CorpAccountReport','OPS Corp Account Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_ERPReport','OPS ERP Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_MirrorAccountReport','OPS Mirror Account Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_IMPSReport','OPS IMPS Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_WalletReport','OPS Wallet Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_RBIReport','OPS RBI Report')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_view','OPS Report panel view')
,('OPS_REPORTPANEL','OPS_REPORTPANEL_superuser','OPS Report Panel SuperUser')

,('OPS_BULKPANEL','OPS_BULKTPANEL_file_manager','OPS Bulk File Manager')
,('OPS_BULKPANEL','OPS_BULKPANEL_superuser','OPS Bulk Panel SuperUser');

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_USERPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_INTERNALPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_REPORTPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_BULKPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));


INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_USERPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='saheb.singh@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_INTERNALPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='saheb.singh@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_REPORTPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='saheb.singh@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_BULKPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='saheb.singh@snapdeal.com'));
