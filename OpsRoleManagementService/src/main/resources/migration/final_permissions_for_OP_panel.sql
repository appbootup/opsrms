use role_mgmt;

DELETE FROM user_permission_mapping WHERE user_id=(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com');
DELETE FROM user_permission_mapping WHERE user_id=(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com');
DELETE FROM user_permission_mapping WHERE user_id=(SELECT id FROM user WHERE user_name='saheb.singh@snapdeal.com');

UPDATE permissions SET display_name="OPS Action Panel Permission Manager" WHERE permission_name="OPS_USERPANEL_superuser";
UPDATE permissions SET display_name="OPS Fraud Management" WHERE permission_name="OPS_USERPANEL_fraudManagemenet";
UPDATE permissions SET display_name="OPS Report Panel Permission Manager" WHERE permission_name="OPS_REPORTPANEL_superuser";
UPDATE permissions SET display_name="OPS Bulk Panel Permission Manager" WHERE permission_name="OPS_BULKPANEL_superuser";

INSERT INTO permissions(app_name,permission_name,display_name) VALUES 
('OPS_KEYPANEL','OPS_KEYPANEL_superuser','OPS Key Panel Permission Manager')
,('OPS_KEYPANEL','OPS_KEYPANEL_view','OPS Key Panel View')
,('OPS_KEYPANEL','OPS_KEYPANEL_ONBOARDING','OPS Key Panel Onboarding')

,('OPS_MOBPANEL','OPS_MOBPANEL_VIEW','OPS MOB Panel View')
,('OPS_MOBPANEL','OPS_MOBPANEL_SUPERUSER','OPS MOB Panel Permission Manager');


INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_INTERNALPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='MANAGE_USER'),
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_USERPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_REPORTPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_BULKPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_KEYPANEL_superuser'),
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_MOBPANEL_SUPERUSER'),
(SELECT id FROM user WHERE user_name='vishal.agarwal@snapdeal.com'));




INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_INTERNALPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='MANAGE_USER'),
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_USERPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_REPORTPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_BULKPANEL_superuser'), 
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_KEYPANEL_superuser'),
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));

INSERT INTO user_permission_mapping VALUES(
(SELECT id FROM permissions WHERE permission_name='OPS_MOBPANEL_SUPERUSER'),
(SELECT id FROM user WHERE user_name='avinash.katiyar@snapdeal.com'));