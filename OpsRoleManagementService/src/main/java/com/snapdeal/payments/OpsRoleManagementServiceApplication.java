package com.snapdeal.payments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath*:spring/application-context.xml")
@SpringBootApplication
@EnableAspectJAutoProxy
@ComponentScan({ "com.snapdeal.*" })
public class OpsRoleManagementServiceApplication {

   public static void main(String[] args) {
      SpringApplication.run(OpsRoleManagementServiceApplication.class, args);
   }
}
