package com.snapdeal.payments;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {

   @Value("${snapdeal.payments.RoleManagement.database.driverClassName}")
   private String driverClassName;

   @Value("${snapdeal.payments.RoleManagement.database.url}")
   private String dbUrl;

   @Value("${snapdeal.payments.RoleManagement.database.username}")
   private String dbUserName;

   @Value("${snapdeal.payments.RoleManagement.database.password}")
   private String dbPassword;

   @Value("${snapdeal.payments.RoleManagement.database.maxactive}")
   private int dbMaxActive;

   @Value("${snapdeal.payments.RoleManagement.database.maxIdle}")
   private int dbMaxIdle;

   @Value("${snapdeal.payments.RoleManagement.database.inititalsize}")
   private int dbInitialSize;

   @Value("${snapdeal.payments.RoleManagement.database.minIdle}")
   private int dbMinIdle;

   @Value("${snapdeal.payments.RoleManagement.database.timebetweenevictionrunsmillis}")
   private int dbTimeBetweenEvictionRunsMillis;

   @Value("${snapdeal.payments.RoleManagement.database.minevictableidletimemillis}")
   private int dbMinEvictableIdleTimeMillis;

   @Value("${snapdeal.payments.RoleManagement.database.maxwait}")
   private int dbDriverMaxWait;

   @Bean
   public DataSource dataSource() {
      BasicDataSource dataSource = new BasicDataSource();
      dataSource.setDriverClassName(driverClassName);
      dataSource.setUrl(dbUrl);
      dataSource.setUsername(dbUserName);
      dataSource.setPassword(dbPassword);
      dataSource.setMaxActive(dbMaxActive);
      dataSource.setMaxIdle(dbMaxIdle);
      dataSource.setInitialSize(dbInitialSize);
      dataSource.setMinIdle(dbMinIdle);
      dataSource.setTimeBetweenEvictionRunsMillis(dbTimeBetweenEvictionRunsMillis);
      dataSource.setMinEvictableIdleTimeMillis(dbMinEvictableIdleTimeMillis);
      dataSource.setMaxWait(dbDriverMaxWait);
      return dataSource;
   }

   /*
    * @Bean
    * public SqlSessionFactory sqlSessionFactory() throws Exception {
    * SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
    * sqlSessionFactory.setDataSource(dataSource());
    * return sqlSessionFactory.getObject();
    * }
    * 
    * @Bean
    * 
    * @Autowired
    * public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory
    * sqlSessionFactory) {
    * SqlSessionTemplate sqlSessionTemplate = new
    * SqlSessionTemplate(sqlSessionFactory,
    * ExecutorType.REUSE);
    * return sqlSessionTemplate;
    * }
    * 
    * @Bean
    * 
    * @Autowired
    * public DataSourceTransactionManager transactionManager(DataSource
    * dataSource) {
    * DataSourceTransactionManager transactionManager = new
    * DataSourceTransactionManager(dataSource);
    * return transactionManager;
    * }
    */

}