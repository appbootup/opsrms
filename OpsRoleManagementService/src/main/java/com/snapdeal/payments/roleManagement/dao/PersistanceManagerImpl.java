package com.snapdeal.payments.roleManagement.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.experimental.Delegate;

@Component
public class PersistanceManagerImpl implements PersistanceManager {

   @Autowired
   @Delegate
   UserDao userDao;

   @Autowired
   @Delegate
   UserPermissionDao userPermissionDao;

   @Autowired
   @Delegate
   UserRoleDao userRoleDao;

   @Autowired
   @Delegate
   OTPDao otpDao;
   
   @Autowired
   @Delegate
   TokenDao tokenDao;
}
