package com.snapdeal.payments.roleManagement.dao;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;

/**
 * @author aniket
 *         15-Feb-2015
 */
public interface TokenDao {

	public TokenDTO getTokenFromUsername(String userName);
	public List<TokenDTO> getTokensFromUsername(String userName);
	public void updateLoginTime(String tokenId);
	public void insertOrUpdateToken(TokenDTO token);
	public TokenDTO getUserNameFromId(String tokenId);
	public void invalidateToken(String tokenId);
	public void insertToken(TokenDTO token);
}