package com.snapdeal.payments.roleManagement.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.common.base.Optional;
import com.snapdeal.payments.roleManagement.dao.model.UserCriteria;
import com.snapdeal.payments.roleManagement.dao.model.UserDaoModel;
import com.snapdeal.payments.roleManagementModel.dto.FreezeAccountDTO;
import com.snapdeal.payments.roleManagementModel.dto.UserVerificationDTO;
import com.snapdeal.payments.roleManagementModel.request.User;

/**
 * @author shubham 19-Oct-2015
 */
public interface UserDao {

	public void saveUser(UserDaoModel user, UserVerificationDTO userVerificationDTO);
	
	public void saveUser(UserDaoModel user);

	public void updateUser(UserDaoModel user);

	public User getUserByUserName(String userName);

	public User getUserById(String userId);

	public List<User> getUsersByRole(String roleName);

	public boolean isUserExist(String userId);

	public Date getPasswordExpiryDate(String userName, String passWord);

	public void resetPassword(String userName, String newPassword, Date passwordExpiresOn);

	public List<User> getAllUsers();

	public List<User> getUsersByIds(List<String> listUserIds);

	public void deleteUser(String userId);

	public Optional<FreezeAccountDTO> getFreezedAccount(String userId);

	public void freezeUser(FreezeAccountDTO freezeAccount);

	public boolean dropFreezedUser(String userId);
	
	public void updateFreezeUser(FreezeAccountDTO freezeAccount);
	
	public String getUserFromCode(String code);
	
	public void changePasswordUsingId(String userId,String newPassword,Date passwordExpiresOn);
	
	public Map<String,UserDaoModel> getSelectedUsers(UserCriteria userCriteria);
	
    public void saveVerificationCode(UserVerificationDTO userVerificationDTO);
	
}
