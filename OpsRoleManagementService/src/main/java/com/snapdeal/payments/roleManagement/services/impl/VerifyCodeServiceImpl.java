package com.snapdeal.payments.roleManagement.services.impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.CipherServiceUtil;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.OTPUtils;
import com.snapdeal.payments.roleManagementModel.exceptions.CipherException;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InvalidCodeException;
import com.snapdeal.payments.roleManagementModel.request.VerifyCodeRequest;
import com.snapdeal.payments.roleManagementModel.services.VerifyCodeService;

import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

/**
 * @author aniket 21-Jan-2016
 */
@Service
@Slf4j
public class VerifyCodeServiceImpl implements VerifyCodeService {

	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	IDFactory idfactory;

	@Autowired
	OTPUtils otpUtils;

	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	@Override
	public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest) {
		String verificationCode = "";
		try {
			verificationCode = CipherServiceUtil.decrypt(verifyCodeRequest.getVerificationCode());
		} catch (CipherException e) {
			throw new InvalidCodeException(ExceptionMessages.INCORRECT_CODE_ERROR);
		}
		String userId = persistanceManager.getUserFromCode(verificationCode);
		if (StringUtils.isEmpty(userId)) {
			throw new InvalidCodeException(ExceptionMessages.INCORRECT_CODE_ERROR);
		}
		String hashedPassword = IDFactory.hashString(verifyCodeRequest.getPassword());
		persistanceManager.changePasswordUsingId(userId, hashedPassword,new DateTime().plusDays(idfactory.getPasswordExpiryInDays()).toDate());
	}

}
