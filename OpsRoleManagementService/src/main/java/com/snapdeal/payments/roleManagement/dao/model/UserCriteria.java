package com.snapdeal.payments.roleManagement.dao.model;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.dto.FieldFilter;
import com.snapdeal.payments.roleManagementModel.dto.RangeFilter;
import com.snapdeal.payments.roleManagementModel.dto.SortField;

import lombok.Data;

@Data
public class UserCriteria {
   List<FieldFilter> filters;
   List<RangeFilter> rangeFilter;
   List<SortField> sortColumns;
   List<String> searchColumns;
   String searchString;
   Integer recordsPerPage;
   Integer offset;
}
