package com.snapdeal.payments.roleManagement.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagementModel.services.ChangePasswordService;
import com.snapdeal.payments.roleManagementModel.services.CreateRoleService;
import com.snapdeal.payments.roleManagementModel.services.CreateUserService;
import com.snapdeal.payments.roleManagementModel.services.DeleteUserService;
import com.snapdeal.payments.roleManagementModel.services.ForgotPasswordNotifyService;
import com.snapdeal.payments.roleManagementModel.services.GenerateOTPService;
import com.snapdeal.payments.roleManagementModel.services.GetRolesByRoleNamesService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByIdService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByTokenService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByUserNameService;
import com.snapdeal.payments.roleManagementModel.services.GetUsersByCriteriaService;
import com.snapdeal.payments.roleManagementModel.services.GetUsersByIdsService;
import com.snapdeal.payments.roleManagementModel.services.GetUsersByRoleService;
import com.snapdeal.payments.roleManagementModel.services.LoginUserService;
import com.snapdeal.payments.roleManagementModel.services.LogoutUserService;
import com.snapdeal.payments.roleManagementModel.services.ResendOTPService;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;
import com.snapdeal.payments.roleManagementModel.services.SocialLoginUserService;
import com.snapdeal.payments.roleManagementModel.services.UiDropDownsService;
import com.snapdeal.payments.roleManagementModel.services.UpdateRoleService;
import com.snapdeal.payments.roleManagementModel.services.UpdateUserService;
import com.snapdeal.payments.roleManagementModel.services.VerifyCodeService;
import com.snapdeal.payments.roleManagementModel.services.VerifyOTPService;

import lombok.experimental.Delegate;

@Component
public class RoleMgmtServiceImpl implements RoleMgmtService {
   @Autowired
   @Delegate
   private CreateRoleService createRoleService;

   @Autowired
   @Delegate
   private UpdateRoleService updateRoleService;

   @Autowired
   @Delegate
   private CreateUserService createUserService;

   @Autowired
   @Delegate
   private UpdateUserService updateUserService;

   @Autowired
   @Delegate
   private GetUserByIdService getUserByIdService;

   @Autowired
   @Delegate
   private GetUserByUserNameService getUserByUserNameService;

   @Autowired
   @Delegate
   private GetUsersByRoleService getUsersByRoleService;

   @Autowired
   @Delegate
   private UiDropDownsService uiDropDownsService;

   @Autowired
   @Delegate
   private ForgotPasswordNotifyService forgotPasswordNotifyService;

   @Autowired
   @Delegate
   private ChangePasswordService changePasswordService;

   @Autowired
   @Delegate
   private LoginUserService loginUserService;

   @Autowired
   @Delegate
   private LogoutUserService logoutUserService;
   
   @Autowired
   @Delegate
   private SocialLoginUserService socialLoginUserService;

   @Autowired
   @Delegate
   private DeleteUserService deleteUserService;
   
   @Autowired
   @Delegate
   private GetUsersByIdsService getUsersByIdsService;
   
   @Autowired
   @Delegate
   private GetRolesByRoleNamesService getRolesByRoleNamesService;
   
   @Autowired
   @Delegate
   private GetUserByTokenService getUserByTokenService;

   @Autowired
   @Delegate
   private GenerateOTPService generateOTPService;
   
   @Autowired
   @Delegate
   private ResendOTPService resendOTPService;
   
   @Autowired
   @Delegate
   private VerifyOTPService verifyOTPService;
   
   @Autowired
   @Delegate
   private VerifyCodeService verifyCodeAndSetPassword;
   
   @Autowired
   @Delegate
   private GetUsersByCriteriaService getUsersByCriteria;
}
