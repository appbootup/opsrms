package com.snapdeal.payments.roleManagement.services.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.handlers.TokenUtils;
import com.snapdeal.payments.roleManagementModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.roleManagementModel.dto.Token;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.UnAuthrizedUserException;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;
import com.snapdeal.payments.roleManagementModel.services.LoginUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 23-Nov-2015
 */
@Service
@Slf4j
public class LoginUserServiceImpl implements LoginUserService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	IDFactory idfactory;

	@Autowired
	TokenUtils tokenUtils;

	@Autowired
	private RequestParamValidatorForRole<LoginUserRequest> requestValidator;

	@Value("${expiry.token}")
	private int expiryDuration;

	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public LoginUserResponse loginUser(LoginUserRequest loginUserRequest) {
		requestValidator.validate(loginUserRequest);
		String userName = loginUserRequest.getUserName();
		String password = loginUserRequest.getPassword();
		String hashedPassword = IDFactory.hashString(password);
		Date expiryDate = persistanceManager.getPasswordExpiryDate(userName, hashedPassword);
		if (null != expiryDate) {
			if (expiryDate.before(new Date())) {
				throw new UnAuthrizedUserException(ExceptionMessages.PASSWORD_EXPIRED_ERROR);
			}
			User user = persistanceManager.getUserByUserName(userName);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR_OF_DAY, expiryDuration);
			TokenDTO tokenObj = new TokenDTO();
			Token token = new Token();
			GenerateTokenResponse response = tokenUtils.generateToken();
			token.setToken(response.getToken());
			tokenObj = new TokenDTO(response.getTokenId(),new Date(),true,userName);
			persistanceManager.insertToken(tokenObj);
			return getResponseFromData(user, token);
		} else {
			throw new UnAuthrizedUserException(ExceptionMessages.LOGIN_FAILURE_EXCEPTION);
		}
	}

	private LoginUserResponse getResponseFromData(User user, Token token) {
		LoginUserResponse response = new LoginUserResponse();
		response.setToken(token);
		response.setUser(user);
		return response;
	}
}