package com.snapdeal.payments.roleManagement.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.dao.model.RolePermissionMapperModel;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.request.UpdateRoleRequest;
import com.snapdeal.payments.roleManagementModel.services.UpdateRoleService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class UpdateRoleServiceImpl implements UpdateRoleService {
   @Autowired
   private PersistanceManager persistanceManager;
   @Autowired
   RequestParamValidatorForRole<UpdateRoleRequest> requestValidator;

   private RolePermissionMapperModel getMapperFromRequest(UpdateRoleRequest request) {
      return new RolePermissionMapperModel(request.getPermissionIds(), request.getId());
   }

	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void updateRole(UpdateRoleRequest updaterole) {
		requestValidator.validate(updaterole);
		persistanceManager.deletePermissionRoleMappingByRoleId(updaterole.getId());
		persistanceManager.savePermissionRoleMapping(getMapperFromRequest(updaterole));

	}

}
