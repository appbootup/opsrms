package com.snapdeal.payments.roleManagement.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagement.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.roleManagementModel.request.Permission;

@Component
public class UserPermissionDaoImpl implements UserPermissionDao {

   @Autowired
   SqlSessionTemplate sqlSession;

   @Override
   public List<Permission> getAllPermissions() {

      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissions");
      return permissions;
   }

   @Override
   public List<Permission> getAllPermissionsByUser(String userName) {
      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissionsByUser",
               userName);
      return permissions;
   }

   @Override
   public List<Permission> getAllPermissionsByRoleId(Integer roleId) {
      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissionsByRole",
               roleId);
      return permissions;
   }

   @Override
   public Map<String, Integer> getAllUserPermissions(String userName) {
      Map<String, String> inputMap = new HashMap<String, String>();
      inputMap.put("userName", userName);

      Map<String, Integer> userPermissionMap = sqlSession.selectMap(
               "permission.getAllPermissionsByUserMap", inputMap, "name");
      return userPermissionMap;
   }

   @Override
   public void mapAllPermissionsWithUser(PermissionUserMapperModel userPermissionMapper) {
      sqlSession.insert("permission.saveRolePermissionMapping", userPermissionMapper);

   }

   @Override
   public void deleteUserPermissionMappingByUser(String userId) {
      sqlSession.delete("permission.deleteUserPermissionMappingByUser", userId);

   }
   
   @Override
   public List<String> getUsersByPermissionCriteria(Set<String> userIds,String columnName,String searchString){
	   Map<String,String> requestObjectForPermissions = new HashMap<String,String>();
	   //requestObject.put("userIds", userIds);
	   requestObjectForPermissions.put("columnName", columnName);
	   requestObjectForPermissions.put("searchString", searchString);
	   List<String> validPermissionsList = new ArrayList<String>();
	   validPermissionsList = sqlSession.selectList("permission.getPermissionsByCriteria",requestObjectForPermissions);
	   Map<String,Object> requestObjectForUserPermissionMapping = new HashMap<String,Object>();
	   requestObjectForUserPermissionMapping.put("userIds", userIds);
	   requestObjectForUserPermissionMapping.put("permissionIds", validPermissionsList);
	   List<String> userByPermissionsList = sqlSession.selectList("permission.getUsersByPermissionCriteria",requestObjectForUserPermissionMapping);
	   return userByPermissionsList;
   }
}
