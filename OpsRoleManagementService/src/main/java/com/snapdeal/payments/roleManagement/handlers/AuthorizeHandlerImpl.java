package com.snapdeal.payments.roleManagement.handlers;

import java.lang.reflect.Method;
import java.text.ParseException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;
import com.snapdeal.payments.roleManagementModel.exceptions.UnAuthrizedException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.interceptor.RequestHeaders;
import com.snapdeal.payments.roleManagementModel.interceptor.RoleMgmtRequestContext;

/**
 * @author shubham
 *         17-Oct-2015
 * 
 *         Handle Authorization for apis
 */
@Component
public class AuthorizeHandlerImpl {

   public static final ExpressionParser parser = new SpelExpressionParser();
   private StandardEvaluationContext context;
   private Method method = null;

   @Autowired
   PermissionHandler permissionHandler;
   
   @Autowired
   PersistanceManager persistanceManager;

   public AuthorizeHandlerImpl() throws InternalServerException {
      context = new StandardEvaluationContext(new PermissionHandler());

      try {
         method = PermissionHandler.class.getMethod("hasPermission", String.class);
      } catch (NoSuchMethodException | SecurityException e) {
         throw new InternalServerException(
                  ExceptionMessages.PERMISSION_INITIALISATION_EXCEPTION);
      }
      context.registerFunction("hasPermission", method);
   }

   public void handleAuthrization(String permissionString) throws UnAuthrizedException,
            ParseException {
      boolean isAuthrized = false;
      String token = RoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
      String tokenId = TokenUtils.getTokenIdFromToken(token);
      TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
      if (null == tokenObj || !tokenObj.getActive()
				|| tokenObj.getLoginTime().before(new DateTime().minusHours(TokenUtils.getDEFAULT_TIMEOUT()).toDate()) )
         throw new ValidationException(ExceptionMessages.SESSION_EXPIRED);
      String permittedExpression = permissionString;
      if (null != permittedExpression && !permittedExpression.isEmpty()) {
         try {
            permissionHandler.populateUserPermissions(tokenObj.getUserName());
            isAuthrized = parse(permittedExpression);
            if (!isAuthrized)
               throw new UnAuthrizedException(ExceptionMessages.UNAUTHORIZED_USER_EXCEPTION);
         } finally {
            permissionHandler.resetUserPermissions();
         }
      }

   }

   public Boolean parse(String permittedExpression) {
      Boolean isAuthrized = null;
      try {
         isAuthrized = parser.parseExpression(permittedExpression).getValue(context, Boolean.class);
      } catch (Throwable th) {
         throw new ValidationException(ExceptionMessages.SYSTEM_ERROR_EXCEPTION);
      }
      return isAuthrized;
   }
}
