package com.snapdeal.payments.roleManagement.services.impl;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagementModel.request.Role;
import com.snapdeal.payments.roleManagementModel.services.GetRoleService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class GetRoleServiceImpl implements GetRoleService {

   @Autowired
   private PersistanceManager persistanceManager;

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public List<Role> getAllRoles() {

		return persistanceManager.getAllRoles();
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public List<Role> getAllRolesByUser(String userName) {
		return persistanceManager.getAllRolesByUser(userName);
	}

}
