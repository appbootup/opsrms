package com.snapdeal.payments.roleManagement.dao.model;

import java.util.Date;
import java.util.List;

import com.snapdeal.payments.roleManagementModel.request.Permission;
import com.snapdeal.payments.roleManagementModel.request.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"password"})
public class UserDaoModel {
   private String id;
   private String name;
   private String email;
   private String mobile;
   private boolean isActive;
   private String userName;
   private String password;
   private boolean isSocialUser;
   private boolean isPasswordSet;
   private Date passwordExpiresOn;
   private List<Permission> permissions;
   private List<Role> roles;
   private Integer count;
}
