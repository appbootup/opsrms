package com.snapdeal.payments.roleManagement.handlers;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;

@Aspect
@Component
public class PreAuthForRoleHandling {

   @Autowired
   AuthorizeHandlerImpl authhandler;

   @Before("within(com.snapdeal..*) && @annotation(authorize)")
   public void handleAuthrization(PreAuthorize authorize) throws Exception {

      authhandler.handleAuthrization(authorize.value());
   }
}
