package com.snapdeal.payments.roleManagement.handlers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagement.dao.PersistanceManager;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Component
public class PermissionHandler {

   @Autowired
   private PersistanceManager userPermissionDao;

   private static ThreadLocal<Map<String, Integer>> userPermissions = new ThreadLocal<>();

   public boolean hasPermission(String permissionName) {

      boolean isPermitted = false;
      if (null != userPermissions.get().get(permissionName))
         isPermitted = true;
      return isPermitted;
   }

   public void populateUserPermissions(String userName) {
      if (userPermissions.get() == null) {
         Map<String, Integer> userPermissionsMap = userPermissionDao.getAllUserPermissions(
        		 userName);
         userPermissions.set(userPermissionsMap);
      }
   }

   public static void resetUserPermissions() {
      userPermissions.remove();
   }
}
