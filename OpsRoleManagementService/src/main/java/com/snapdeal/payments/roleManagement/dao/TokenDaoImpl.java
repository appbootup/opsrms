package com.snapdeal.payments.roleManagement.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;

@Component
public class TokenDaoImpl implements TokenDao {

	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public TokenDTO getTokenFromUsername(String userName) {
		TokenDTO token = sqlSession.selectOne("user_token.getTokenFromUsername", userName);
		return token;
	}
	@Override
	public List<TokenDTO> getTokensFromUsername(String userName) {
        List<TokenDTO> token = sqlSession.selectList("user_token.getTokenFromUsername", userName);
        return token;
    }
	@Override
	public void updateLoginTime(String tokenId){
		sqlSession.update("user_token.udpateLoginTime", tokenId);
	}

	@Override
	public void insertOrUpdateToken(TokenDTO token) {
		sqlSession.insert("user_token.insertOrUpdateToken", token);
	}
	
	@Override
    public void insertToken(TokenDTO token) {
        sqlSession.insert("user_token.insertToken", token);
    }

	@Override
	public TokenDTO getUserNameFromId(String tokenId) {
		TokenDTO token = sqlSession.selectOne("user_token.getUserNameFromId", tokenId);
		return token;
	}

	@Override
	public void invalidateToken(String tokenId) {
		sqlSession.update("user_token.invalidateToken", tokenId);
		
	}
	
}