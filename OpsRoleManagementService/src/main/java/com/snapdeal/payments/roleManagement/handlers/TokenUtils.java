package com.snapdeal.payments.roleManagement.handlers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagementModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;

import lombok.Getter;

@Component
public class TokenUtils {
	@Value("${version.token}")
	private String version;
	
	@Getter
	private static int DEFAULT_TIMEOUT = 12;


	public GenerateTokenResponse generateToken() {
		StringBuilder tokenBuilder = new StringBuilder();
		String tokenId = UUID.randomUUID().toString();
		tokenBuilder.append(version + "#" + tokenId);
		String hash = IDFactory.hashString(tokenBuilder.toString());
		tokenBuilder.append("#" + hash);
		String token = CipherServiceUtil.encrypt(tokenBuilder.toString());
		return new GenerateTokenResponse(tokenId,token);
	}
	
	public String generateToken(String tokenId){
		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(version + "#" + tokenId);
		String hash = IDFactory.hashString(tokenBuilder.toString());
		tokenBuilder.append("#" + hash);
		String token = CipherServiceUtil.encrypt(tokenBuilder.toString());
		return token;
	}
	
	public static String decryptToken(String t) {
		return new String(CipherServiceUtil.decrypt(t));
	}

	public static String getTokenIdFromToken(String t) {
		String tokenString = decryptToken(t);
		String[] parts = tokenString.split("#");
		if (parts.length != 3)
			throw new ValidationException(ExceptionMessages.SESSION_EXPIRED);
		else 
			tokenString = parts[0].trim()+ "#" + parts[1].trim();
		if(!(IDFactory.hashString(tokenString).equals(parts[2].trim()))){
			throw new ValidationException(ExceptionMessages.SESSION_EXPIRED);
		}
		return parts[1].trim();
	}

}
