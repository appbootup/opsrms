package com.snapdeal.payments.roleManagement.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyOTPRequest;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class OTPController extends AbstractController {

	@Autowired
	private RoleMgmtService roleMgmtService;

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.GENERATE_OTP, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> generateOtp(@RequestBody GenerateOTPRequest request) {

		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = roleMgmtService.generateOTP(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.RESEND_OTP, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> reSendOTP(@RequestBody ResendOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = roleMgmtService.resendOTP(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.VALIDATE_OTP, method = RequestMethod.POST)
	public ServiceResponse<Void> verifyOTP(@RequestBody VerifyOTPRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.verifyOTP(request);
		return response;
	}

	@ExceptionHandler(RoleMgmtException.class)
	@Logged
	public <T> ServiceResponse<T> handleException(RoleMgmtException exception) {
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException(exception.getMessage());
		roleException.setErrorCode(exception.getErrorCode());
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(Exception.class)
	@Logged
	public <T> ServiceResponse<T> handleException(Exception exception) {
		log.info("Exception" + exception.toString());
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(RuntimeException.class)
	@Logged
	public <T> ServiceResponse<T> handleException(RuntimeException exception) {
		log.info("Runtime Exception" + exception.toString());
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}
}
