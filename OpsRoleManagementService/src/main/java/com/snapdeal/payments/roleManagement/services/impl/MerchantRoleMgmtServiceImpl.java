package com.snapdeal.payments.roleManagement.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.base.Optional;
import com.snapdeal.payments.communicator.dto.CommunicationIntent;
import com.snapdeal.payments.communicator.exception.HttpTransportException;
import com.snapdeal.payments.communicator.exception.ServiceException;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.CipherServiceUtil;
import com.snapdeal.payments.roleManagement.handlers.CommunicatorUtils;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.OTPUtils;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.exceptions.CipherException;
import com.snapdeal.payments.roleManagementModel.exceptions.EmailAndMobileNotPresentException;
import com.snapdeal.payments.roleManagementModel.exceptions.EmailIdNotPresentException;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPExpiredException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPIdInvalidException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPInvalidAttemptsExceededException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPServiceException;
import com.snapdeal.payments.roleManagementModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.roleManagementModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.roleManagementModel.response.FrozenAccountResponse;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.response.SendEmailAndSmsOnMerchantCreationResponse;
import com.snapdeal.payments.roleManagementModel.response.VerifyOtpForMobileVerificationResponse;
import com.snapdeal.payments.roleManagementModel.services.MerchantRoleMgmtService;

import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.snapdeal.payments.roleManagementModel.commons.OTPConstants;
import com.snapdeal.payments.roleManagementModel.commons.OTPState;
import com.snapdeal.payments.roleManagementModel.dto.FreezeAccountDTO;
import com.snapdeal.payments.roleManagementModel.dto.GenerateOtpServiceForMerchantDTO;
import com.snapdeal.payments.roleManagementModel.dto.UserOTPDTO;
import com.snapdeal.payments.roleManagementModel.dto.UserVerificationDTO;
import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;

@Service
@Slf4j
@Component
public class MerchantRoleMgmtServiceImpl implements MerchantRoleMgmtService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<ResendOTPRequest> requestValidatorForResend;

	@Autowired
	private RequestParamValidatorForRole<GenerateOTPRequest> requestValidator;
	
	@Autowired
	private RequestParamValidatorForRole<SetPasswordToDummyPasswordRequest> requestValidatorForDummyPassword;

	@Autowired
	private RequestParamValidatorForRole<VerifyOtpForMobileVerificationRequest> requestValidatorForOtpVerification;

	@Value("${version.verificationCode}")
	private String verificationCodeVersion;

	@Value("${application.environment}")
	private String environment;

	@Value("${verificationCode.expiryInHours}")
	@Getter
	@Setter
	private Integer expiryDurationInHours;

	@Autowired
	OTPUtils otpUtils;

	@Autowired
	CommunicatorUtils communicatorUtils;
	
	@Autowired
	IDFactory idfactory;

	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@Override
	public SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreation(SendEmailAndSmsOnMerchantCreationRequest request) {
		if (!persistanceManager.isUserExist(request.getUserID())) {
			log.info("User does not exist {} ", request.getUserID());
			throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		}

		User user = persistanceManager.getUserById(request.getUserID());
		String email = user.getEmail();
		String mobile = user.getMobile();
		String userName = user.getUserName();
		String userID =request.getUserID();
		
		if (StringUtils.isBlank(email) && StringUtils.isBlank(mobile)) {
			log.info("Mobile number and Email Id are not registered with us for userName: {}",userName);
			throw new EmailAndMobileNotPresentException(ExceptionMessages.EMAIL_AND_MOBILE_NOT_PRESENT);
		}
		
		if (StringUtils.isBlank(email)) {
			log.info("Email Id does not exist in the system for userID {} ", userID);
		}
		if (StringUtils.isBlank(mobile)) {
			log.info("Mobile number is not present in the system for userId {} ", userID);
			
		}
		
		boolean sent_mail_status = false;
		String encryptedVerificationCode = null;
		try {
			String verificationCode = getVerificationCode();
			persistanceManager.saveVerificationCode(getUserVerificationDTO(userID, verificationCode));
			encryptedVerificationCode = CipherServiceUtil.encrypt(verificationCode);
		} catch (CipherException e) {
			log.error("Unable to encrypt verification code", e);
			throw new InternalServerException(ExceptionMessages.SYSTEM_ERROR_EXCEPTION);
		}

		StringBuilder url = new StringBuilder(request.getLinkForSettingPassword());
		url.append(encryptedVerificationCode);
		try {
			CommunicationIntent intent = communicatorUtils.generateCommunicationIntentForMerchant(url.toString(),
					email,mobile,userName);
			log.info("Sending communication intent for userID: {} : {}", userID, intent.toString());
			communicatorUtils.sendCommunicationIntent(intent);
			sent_mail_status = true;
		} catch (HttpTransportException ex) {
			log.error("HttpTransportException while sending separate communication intent for userID: {} "
					+ "Exception: {}", userID, ex);
		} catch (ServiceException ex) {
			log.error(
					"ServiceException while sending separate communication intent for userID: {} " + "Exception: {}",
					userID, ex);
		} catch (Exception ex) {
			log.error("Exception while sending separate communication intent for userID: {} " + "Exception: {}",
					userID, ex);
		}

		return new SendEmailAndSmsOnMerchantCreationResponse(sent_mail_status);
	}
	
	

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GenerateOTPResponse sendOtpForMobileVerification(GenerateOTPRequest generateOtpRequest) {
		requestValidator.validate(generateOtpRequest);
		User user = persistanceManager.getUserByUserName(generateOtpRequest.getUserName());
		if (null == user) {
			log.info("User does not exist! {} ", generateOtpRequest.getUserName());
			throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		}
		String userId = user.getId();
		String name = user.getName();
		String mobile = user.getMobile();
		String email = user.getEmail();
		if (StringUtils.isBlank(email) && StringUtils.isBlank(mobile)) {
			log.info("Mobile number and Email Id are not registered with us for userId: {}",
					userId);
			throw new EmailAndMobileNotPresentException(ExceptionMessages.EMAIL_AND_MOBILE_NOT_PRESENT);
		}
		if (StringUtils.isBlank(mobile)) {
			log.info("Mobile number is not registered with us for userId: {}", userId);
		}
		if (StringUtils.isBlank(email)) {
			log.info("Email Id is not present for userId: {}", userId);
		}
		otpUtils.verifyFrozenAccountForMerchant(userId);
		UserOTPDTO otpInfo = generateOTP(user);
		CommunicationIntent intent = communicatorUtils.generateSMSCommunicationIntent(email, mobile, otpInfo.getOtp(),
				name);
		try {
			log.info("Sending communication intent for userId: {} : {}", userId, intent.toString());
			communicatorUtils.sendCommunicationIntent(intent);
		} catch (HttpTransportException ex) {
			log.error("HttpTransportException while sending communication intent for userId: {}", userId,
					ex);
		} catch (ServiceException ex) {
			log.error("ServiceException while sending communication intent for userId: {} ", userId, ex);
		} catch (Exception ex) {
			log.error("Exception while sending communication intent for userId: {} ,exception message : {}", userId,
					ex);
		}

		
		GenerateOTPResponse response = new GenerateOTPResponse(otpInfo.getOtpId());
		log.info("GenerateOTPResponse for userId: {} is {} ", userId, response);
		return response;
	}

	
	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GenerateOTPResponse sendDummyOtpForMobileVerification(GenerateOTPRequest generateOtpRequest) {
		
		if (!environment.equals("STAGING")) {
			log.error("Environment is not staging.Cannot generate dummy OTP for userName {} ",generateOtpRequest.getUserName());
			throw new OTPServiceException(ExceptionMessages.UNAUTHORIZED_USER_EXCEPTION);
		}
		log.info("Environment is STAGING.Generate dummy OTP method is called for userName: {}", generateOtpRequest.getUserName());
		requestValidator.validate(generateOtpRequest);
		User user = persistanceManager.getUserByUserName(generateOtpRequest.getUserName());
		if (null == user) {
			log.info("User does not exist! {} ", generateOtpRequest.getUserName());
			throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		}
		String userId = user.getId();
		String name = user.getName();
		String mobile = user.getMobile();
		String email = user.getEmail();
		if (StringUtils.isBlank(email) && StringUtils.isBlank(mobile)) {
			log.info("Mobile number and Email Id are not registered with us for userId: {}",
					userId);
			throw new EmailAndMobileNotPresentException(ExceptionMessages.EMAIL_AND_MOBILE_NOT_PRESENT);
		}
		if (StringUtils.isBlank(mobile)) {
			log.info("Mobile number is not registered with us for userId: {}", userId);
		}
		if (StringUtils.isBlank(email)) {
			log.info("Email Id is not present for userId: {}",userId);
		}
		otpUtils.verifyFrozenAccountForMerchant(userId);
		UserOTPDTO otpInfo = generateDummyOTP(user);
		CommunicationIntent intent = communicatorUtils.generateSMSCommunicationIntent(email, mobile, otpInfo.getOtp(),
				name);
		try {
			log.info("Sending communication intent for userId: {} : {}", userId, intent.toString());
			communicatorUtils.sendCommunicationIntent(intent);
		} catch (HttpTransportException ex) {
			log.error("HttpTransportException while sending communication intent for userId: {}", userId,
					ex);
		} catch (ServiceException ex) {
			log.error("ServiceException while sending communication intent for userId: {} ", userId, ex);
		} catch (Exception ex) {
			log.error("Exception while sending communication intent for userId: {} ,exception message : {}", userId,
					ex);
		}

		GenerateOTPResponse response = new GenerateOTPResponse(otpInfo.getOtpId());
		log.info("Dummy GenerateOTPResponse for userId: {} is {} ", userId, response);
		return response;
	}

	
	
	
	
	private UserOTPDTO generateOTP(User user) {
		Optional<UserOTPDTO> otpOption = persistanceManager.getLatestOTP(user.getId());
		if (otpOption.isPresent()) {
			UserOTPDTO otpInfo = otpOption.get();
			OTPState currentOtpState = OTPUtils.getOtpState(otpOption);
			switch (currentOtpState) {
			case VERIFIED:
			case NOT_ACTIVE:
				otpUtils.updateOTPState(otpInfo);
				return otpUtils.generateOTPInfoForMobileVerification(
						new GenerateOtpServiceForMerchantDTO(user.getId(), user.getMobile(), user.getEmail()));
			case IN_EXPIRY:
				UserOTPDTO otp = otpUtils.updateResendAttemptsForMerchant(otpOption);
				otpUtils.incrementSendCount(otp);
				return otp;
			default:
				throw new OTPServiceException(ExceptionMessages.OTP_GENERIC_EXCEPTION);
			}
		} else {
			log.info("Generating new OTP for merchant");
			return otpUtils.generateOTPInfoForMobileVerification(
					new GenerateOtpServiceForMerchantDTO(user.getId(), user.getMobile(), user.getEmail()));
		}

	}
	
	private UserOTPDTO generateDummyOTP(User user) {
		log.info("Generating dummy OTP for merchant");
		Optional<UserOTPDTO> otpOption = persistanceManager.getLatestOTP(user.getId());
		if (otpOption.isPresent()) {
			UserOTPDTO otpInfo = otpOption.get();
			OTPState currentOtpState = OTPUtils.getOtpState(otpOption);
			switch (currentOtpState) {
			case VERIFIED:
			case NOT_ACTIVE:
				otpUtils.updateOTPState(otpInfo);
				return otpUtils.generateDummyOTPInfoForMobileVerification(
						new GenerateOtpServiceForMerchantDTO(user.getId(), user.getMobile(), user.getEmail()));
			case IN_EXPIRY:
				UserOTPDTO otp = otpUtils.updateResendAttemptsForMerchant(otpOption);
				otpUtils.incrementSendCount(otp);
				return otp;
			default:
				throw new OTPServiceException(ExceptionMessages.OTP_GENERIC_EXCEPTION);
			}
		} else {
			log.info("Generating new OTP for merchant");
			return otpUtils.generateDummyOTPInfoForMobileVerification(
					new GenerateOtpServiceForMerchantDTO(user.getId(), user.getMobile(), user.getEmail()));
		}

	}

	private String getVerificationCode() {
		String uuid = UUID.randomUUID().toString();
		uuid = verificationCodeVersion + "#" + uuid;
		return uuid;
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GenerateOTPResponse resendOtpForMobileVerification(ResendOTPRequest resendOTPRequest) {

		requestValidatorForResend.validate(resendOTPRequest);
		Optional<UserOTPDTO> currentOtpInfo = persistanceManager.getOTPFromId(resendOTPRequest.getOtpId());
		if (currentOtpInfo.isPresent()) {
			otpUtils.verifyFrozenAccountForMerchant(currentOtpInfo.get().getUserId());
			OTPState currentOtpState = OTPUtils.getOtpState(currentOtpInfo);
			UserOTPDTO otpInfo = getUserOTPEntity(currentOtpState, currentOtpInfo);
			String mobile = currentOtpInfo.get().getMobileNumber();
			String email = currentOtpInfo.get().getEmail();
			String userID = currentOtpInfo.get().getUserId();
			String name = persistanceManager.getUserById(userID).getName();
			CommunicationIntent intent = communicatorUtils.generateSMSCommunicationIntent(email, mobile,
					otpInfo.getOtp(), name);
			try {
				log.info("Resending communication intent for userId: {} : {}", userID, intent.toString());
				communicatorUtils.sendCommunicationIntent(intent);
			} catch (HttpTransportException ex) {
				log.error("HttpTransportException while resending communication intent for userId: {}", userID,
						ex);
			} catch (ServiceException ex) {
				log.error("ServiceException while resending communication intent for userId: {} ", userID,
						ex);
			} catch (Exception ex) {
				log.error("Exception while sending communication intent for userId: {} ,exception message : {}", userID,
						ex);
			}
			GenerateOTPResponse response = new GenerateOTPResponse(otpInfo.getOtpId());
			log.info("GenerateOTPResponse for ResendOTPRequest for userId: {} is {} ", userID, response);
			return response;
		} else {
			throw new OTPIdInvalidException(ExceptionMessages.INVALID_OTP_ID);
		}
	}

	private UserOTPDTO getUserOTPEntity(OTPState currentOtpState, Optional<UserOTPDTO> currentOtpInfo) {
		switch (currentOtpState) {
		case DOES_NOT_EXIST:
			throw new OTPIdInvalidException(ExceptionMessages.INVALID_OTP_ID);
		case NOT_ACTIVE:
			otpUtils.updateOTPState(currentOtpInfo.get());
			return otpUtils.generateOTPInfoForMobileVerification(
					new GenerateOtpServiceForMerchantDTO(currentOtpInfo.get().getUserId(),
							currentOtpInfo.get().getMobileNumber(), currentOtpInfo.get().getEmail()));
		case IN_EXPIRY:
			UserOTPDTO otp = otpUtils.updateResendAttemptsForMerchant(currentOtpInfo);
			otpUtils.incrementSendCount(otp);
			return otp;
		default:
			throw new OTPServiceException(ExceptionMessages.OTP_GENERIC_EXCEPTION);
		}
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerification(
			VerifyOtpForMobileVerificationRequest request) {

		boolean isValidOtp = false;
		int status;
		requestValidatorForOtpVerification.validate(request);
		Optional<UserOTPDTO> currentOtpInfo = persistanceManager.getOTPFromId(request.getOtpId());

		if (currentOtpInfo.isPresent()) {
			otpUtils.verifyFrozenAccountForMerchant(currentOtpInfo.get().getUserId());
			status = verify(currentOtpInfo, request);
		} else {
			log.info("Otp Id is invalid :{}", request.getOtpId());
			throw new OTPIdInvalidException(ExceptionMessages.INVALID_OTP_ID);
		}
		if (status == OTPConstants.STATUS) {
			log.info("Otp is valid for the otpId :{}", request.getOtpId());
			isValidOtp = true;
		} else {
			isUserFrozen(currentOtpInfo.get());
			log.info("Invalid otp entered for otpId {} ", request.getOtpId());
		}
		return new VerifyOtpForMobileVerificationResponse(isValidOtp);

	}

	private int verify(Optional<UserOTPDTO> currentOtpInfo, VerifyOtpForMobileVerificationRequest request) {

		int status = -1;
		FrozenAccountResponse frozenAccountResponse = isUserFrozen(currentOtpInfo.get());
		if (activeOrNot(currentOtpInfo)) {
			if (currentOtpInfo.get().getOtp().equalsIgnoreCase(request.getOtp())) {
				otpUtils.updateOTPState(currentOtpInfo.get());
				if (frozenAccountResponse.isStatus() == true
						&& frozenAccountResponse.getRequestType().equals(OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS)) {
					persistanceManager.dropFreezedUser(currentOtpInfo.get().getUserId());
				}
				status = 200;

			} else {
				otpUtils.updateInvalidAttempts(currentOtpInfo);
				log.info("count for invalid attempts {} ", currentOtpInfo.get().getInvalidAttempts());
				log.info(" invalid attempts allowed {} ", otpUtils.getInvalidAttemptsLimit());
				if (currentOtpInfo.get().getInvalidAttempts() >= otpUtils.getInvalidAttemptsLimit()) {
					if (frozenAccountResponse.isStatus() == true && frozenAccountResponse.getRequestType()
							.equals(OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS)) {
						otpUtils.updateBlockUser(currentOtpInfo.get());
					} else {
						otpUtils.blockUser(currentOtpInfo.get(), OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS);
					}

				}
				log.info("Invalid Otp is entered for userId ", currentOtpInfo.get().getUserId());
			}
			return status;
		} else {
			log.info("OTP has expired for userId ", currentOtpInfo.get().getUserId());
			throw new OTPExpiredException(ExceptionMessages.EXPIRED_OTP_ERROR);
		}
	}

	private boolean activeOrNot(Optional<UserOTPDTO> currentOtpInfo) {
		String state = OTPUtils.getOtpState(currentOtpInfo).toString();
		if (state.equalsIgnoreCase("ACTIVE") || state.equalsIgnoreCase("IN_THRESHOLD")
				|| state.equalsIgnoreCase("IN_EXPIRY")) {
			return true;
		}
		return false;
	}

	private FrozenAccountResponse isUserFrozen(UserOTPDTO otp) {
		Optional<FreezeAccountDTO> frozenAccountEntity = persistanceManager.getFreezedAccount(otp.getUserId());
		FrozenAccountResponse frozenAccountResponse = OTPUtils.calculateFrozenAccountResponse(frozenAccountEntity);
		if (frozenAccountResponse.isStatus() == true && frozenAccountResponse.getRequestType()
				.equalsIgnoreCase(OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS)) {
			throw new OTPInvalidAttemptsExceededException(MessageFormat.format(
					ExceptionMessages.VERIFY_OTP_LIMIT_BREACHED, frozenAccountResponse.getRemainingMinutes() / 1));
		}
		return frozenAccountResponse;
	}

	private UserVerificationDTO getUserVerificationDTO(String userId, String code) {
		return new UserVerificationDTO(code, userId,
				new DateTime().plusMinutes(getExpiryDurationInHours() * 60).toDate(), new Date());
	}


	// Adding this code for merchant_revamp requirement CS-212
	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public Boolean resetPasswordToDummyPasswordForStaging(SetPasswordToDummyPasswordRequest request) {

		boolean isPasswordSet = false;
		requestValidatorForDummyPassword.validate(request);
		log.info("SetPasswordToDummyPasswordRequest for userName {}",request.getUserName());
        log.info("Environment is {}",environment);

		if (environment.equals("STAGING")) {
			try {
			    String newPassword = request.getPassword();
				String hashedPassword = IDFactory.hashString(newPassword);
				persistanceManager.resetPassword(request.getUserName(), hashedPassword,
						new DateTime().plusDays(idfactory.getPasswordExpiryInDays()).toDate());
				isPasswordSet = true;
				log.info("Password has been set successfully !");
			} catch (Exception e) {
				log.error("Error connecting to database!");
			}
		}
		else {
			log.info("Environment is not staging,cannot reset password to dummy password!!");
		}
	
		log.info("SetPasswordToDummyPassword Response for userName: {} is {} ", request.getUserName(), isPasswordSet);
		return new Boolean(isPasswordSet);

	}

}