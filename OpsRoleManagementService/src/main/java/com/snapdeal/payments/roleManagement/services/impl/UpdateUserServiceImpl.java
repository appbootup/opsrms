package com.snapdeal.payments.roleManagement.services.impl;

import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.roleManagement.dao.model.RoleUserMapperModel;
import com.snapdeal.payments.roleManagement.dao.model.UserDaoModel;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.request.GetUserByIdRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.roleManagementModel.request.UpdateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.GetUserByIdResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.roleManagementModel.services.GetUserByIdService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByTokenService;
import com.snapdeal.payments.roleManagementModel.services.UpdateUserService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class UpdateUserServiceImpl implements UpdateUserService {
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<UpdateUserRequest> requestValidator;
   
    @Autowired
	@Qualifier("getUserByToken")
	GetUserByTokenService getUserByTokenService;
	
	@Autowired
	@Qualifier("getUserById")
	GetUserByIdService getUserByIdService;

	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void updateUser(UpdateUserRequest user) {
		requestValidator.validate(user);
		verifyIfUserExist(user.getUserId());
		persistanceManager.updateUser(getUserDaoFromRequest(user));
		if (null != user.getPermissionIds() && !user.getPermissionIds().isEmpty()) {
			persistanceManager.deleteUserPermissionMappingByUser(user.getUserId());
			persistanceManager.mapAllPermissionsWithUser(getPermissionMapperFromRequest(user));
		}

		if (null != user.getRoleIds() && !user.getRoleIds().isEmpty()) {
			persistanceManager.deleteUserRoleMappingByUser(user.getUserId());
			persistanceManager.addUserRoleMapping(getRoleMapperFromRequest(user));
		}
		
		
		try {
			String appName = user.getAppName();
			String token = user.getRequestToken();
			String userIdCallingtheAPI = null;
			String userNameCallingtheAPI = null;
			String NameCallingtheAPI = null;
			if(!StringUtils.isBlank(token)) {
			GetUserByTokenRequest req = new GetUserByTokenRequest();
			req.setRequestToken(token);
			GetUserByTokenResponse res = getUserByTokenService.getUserByToken(req);
			User user_calling_the_API = res.getUser();
			userIdCallingtheAPI = user_calling_the_API.getId();
			userNameCallingtheAPI = user_calling_the_API.getUserName();
			NameCallingtheAPI = user_calling_the_API.getName();
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String time = dateFormat.format(date);
			GetUserByIdRequest getUserByIdRequest = new GetUserByIdRequest();
			getUserByIdRequest.setUserId(user.getUserId());
			GetUserByIdResponse getUserByIdResponse = getUserByIdService.getUserById(getUserByIdRequest);
			User userInfoAfterUpdating = getUserByIdResponse.getUser();
			log.info("USER_UPDATED : UpdateUserServiceImpl is called with appNameCallingtheAPI : {} ,userIdCallingtheAPI : {} ,userNameCallingtheAPI : {} ,NameCallingtheAPI : {} ,updatedOn : {} ,userIDGettingUpdated : {} ,"
						+ "userInfoAfterUpdating : {} ",appName,userIdCallingtheAPI,userNameCallingtheAPI,NameCallingtheAPI,time,user.getUserId(),userInfoAfterUpdating);
		}
		catch (Exception e) {
			log.error("Error in logging data on user updation! ",e);
		}
	}

   private void verifyIfUserExist(String userId) {
      if (!persistanceManager.isUserExist(userId))
         throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
   }

   private UserDaoModel getUserDaoFromRequest(UpdateUserRequest request) {
      UserDaoModel user = new UserDaoModel();
      user.setId(request.getUserId());
      user.setMobile(request.getMobile());
      user.setName(request.getName());
      user.setActive(request.isActive());
      return user;
   }

   private PermissionUserMapperModel getPermissionMapperFromRequest(UpdateUserRequest request) {
      return new PermissionUserMapperModel(request.getPermissionIds(), request.getUserId());
   }

   private RoleUserMapperModel getRoleMapperFromRequest(UpdateUserRequest request) {
      return new RoleUserMapperModel(request.getRoleIds(), request.getUserId());
   }

}
