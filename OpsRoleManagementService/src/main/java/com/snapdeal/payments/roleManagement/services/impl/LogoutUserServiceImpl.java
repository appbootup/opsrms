package com.snapdeal.payments.roleManagement.services.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.handlers.TokenUtils;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.LogoutUserRequest;
import com.snapdeal.payments.roleManagementModel.services.LogoutUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket
 *         16-Feb-2016
 */
@Service
@Slf4j
public class LogoutUserServiceImpl implements LogoutUserService {
   @Autowired
   private PersistanceManager persistanceManager;
   
   @Autowired
   private RequestParamValidatorForRole<LogoutUserRequest> requestValidator;


	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void logoutUser(LogoutUserRequest logoutUserRequest) {
	    String token = logoutUserRequest.getRequestToken();
	    if(StringUtils.isBlank(token)){
	    	throw new ValidationException(ExceptionMessages.SESSION_EXPIRED);
	    }
	    String tokenId = TokenUtils.getTokenIdFromToken(token);
		log.info("The tokenId is {}",tokenId);
		
		TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
		log.info("The token active value before invalidating session is  {}",tokenObj.getActive());
	    persistanceManager.invalidateToken(tokenId);
		 tokenObj = persistanceManager.getUserNameFromId(tokenId);
		log.info("The token active after invalidating session is  {}",tokenObj.getActive());
	}

}
