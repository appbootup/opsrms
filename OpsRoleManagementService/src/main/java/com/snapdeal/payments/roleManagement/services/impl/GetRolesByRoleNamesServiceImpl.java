package com.snapdeal.payments.roleManagement.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.roleManagementModel.request.Role;
import com.snapdeal.payments.roleManagementModel.response.GetRolesByRoleNamesResponse;
import com.snapdeal.payments.roleManagementModel.services.GetRolesByRoleNamesService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 
 * 11-Dec-2015
 */
@Service
@Slf4j
public class GetRolesByRoleNamesServiceImpl implements GetRolesByRoleNamesService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	RequestParamValidatorForRole<GetRolesByRoleNamesRequest> requestValidator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.snapdeal.payments.roleManagement.services.GetRolesByRoleNames#
	 * getRolesByRoleNames
	 * (com.snapdeal.payments.roleManagementModel.request.GetRolesByRoleNamesRequest)
	 */
	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetRolesByRoleNamesResponse getRolesByRoleNames(GetRolesByRoleNamesRequest getRolesByRoleNames) {
		requestValidator.validate(getRolesByRoleNames);
		return getResponseFromData(persistanceManager.getRolesByRoleNames(getRolesByRoleNames.getListRoleNames()));
	}


	private GetRolesByRoleNamesResponse getResponseFromData(List<Role> data) {
		return new GetRolesByRoleNamesResponse(data);
	}

}
