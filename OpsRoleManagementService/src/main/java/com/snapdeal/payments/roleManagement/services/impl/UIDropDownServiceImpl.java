package com.snapdeal.payments.roleManagement.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagementModel.request.GetAllPermissionsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllRolesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllUsersRequest;
import com.snapdeal.payments.roleManagementModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllRolesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllUsersResponse;
import com.snapdeal.payments.roleManagementModel.services.UiDropDownsService;

@Service
@Slf4j
public class UIDropDownServiceImpl implements UiDropDownsService {

   @Autowired
   private PersistanceManager persistanceManager;

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllRolesResponse getAllRoles(GetAllRolesRequest request) {
		return new GetAllRolesResponse(persistanceManager.getAllRoles());
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request) {
		return new GetAllPermissionsResponse(persistanceManager.getAllPermissions());
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {

		return new GetAllUsersResponse(persistanceManager.getAllUsers());
	}

}
