package com.snapdeal.payments.roleManagement.handlers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import com.snapdeal.payments.roleManagementModel.exceptions.CipherException;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;

/**
 * Utility class to encrypt and decrypt.
 *
 */
public class CipherServiceUtil {
	
	private static final String uniqueKey = "U25hcGRlYWxVbmlxdWVLZXk=";
	
	private static final String KEY_ALGORITHM_V1 = "AES";

	private static final String PASSWORD_HASH_ALGORITHM = "SHA-256";

	private static Cipher encryptCipher;
	private static Cipher decryptCipher;

	public CipherServiceUtil() {
		
	}
	static {
		try {
			Key secretKeyV1 = buildKeyAES(uniqueKey, KEY_ALGORITHM_V1);
			encryptCipher = Cipher.getInstance(secretKeyV1.getAlgorithm());
			decryptCipher = Cipher.getInstance(secretKeyV1.getAlgorithm());
			// Set the mode for encrypt and decrypt.
			encryptCipher.init(Cipher.ENCRYPT_MODE, secretKeyV1);
			decryptCipher.init(Cipher.DECRYPT_MODE, secretKeyV1);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException
				| UnsupportedEncodingException | InvalidKeyException e) {
			throw new InternalServerException(ExceptionMessages.CIPHER_ERROR);
		}

	}

	private static Key buildKeyAES(String password, String algorithm)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digester = MessageDigest
				.getInstance(PASSWORD_HASH_ALGORITHM);
		digester.reset();
		digester.update(password.getBytes("UTF-8"));
		byte[] key = digester.digest();
		SecretKeySpec spec = new SecretKeySpec(key,0,16, algorithm);
		return spec;
	}

	public static String decrypt(String token) throws CipherException {
		byte[] decodedToken = org.apache.commons.codec.binary.Base64.decodeBase64(token);
		byte[] decryptedToken = null;
		try {
			decryptedToken = decryptCipher.doFinal(decodedToken);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CipherException(ExceptionMessages.DECRTYPTION_ERROR);
		}
		String textDecryptped = new String(decryptedToken);
		return textDecryptped;
	}

	public static String encrypt(String uniqueId) throws CipherException {
		byte[] text = uniqueId.getBytes();
		byte[] textEncrypted;
		try {
			textEncrypted = encryptCipher.doFinal(text);
			byte[] token =org.apache.commons.codec.binary.Base64.encodeBase64URLSafe(textEncrypted);
			return new String(token);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new CipherException(ExceptionMessages.ENCRYPTION_ERROR);
		}
	}

}