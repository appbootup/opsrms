package com.snapdeal.payments.roleManagement.handlers;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.time.DateUtils;

@Component
public class IDFactory {
	
	@Value("${password.expiryInDays}")
	@Getter
	@Setter
	private Integer passwordExpiryInDays;

	private final static long TRUNCATE_ZEROS = 100000;

	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		String encodedUUID = Base64.encodeBase64URLSafeString(bb.array());
		return encodedUUID;
	}

	public static Long getHourGranularity() {
		Calendar calender = Calendar.getInstance();
		calender = DateUtils.truncate(calender, Calendar.HOUR);
		return calender.getTimeInMillis() / TRUNCATE_ZEROS;
	}

	public static String hashString(String stringToHash) {

		String hashedString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] bytes = md.digest(stringToHash.getBytes());
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			hashedString = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new InternalServerException(ExceptionMessages.PASSWORD_HASHING_EXCEPTION);
		}
		return hashedString;

	}

	public static String forgotPasswordNotify() {
		List<Character> chars = new ArrayList<>();
		Random rand = new Random();
		for (int i = 0; i < 1; i++)
			chars.add((char) ('0' + rand.nextInt(10)));
		for (int i = 0; i < 2; i++)
			chars.add((char) ('a' + rand.nextInt(26)));
		for (int i = 0; i < 1; i++)
			chars.add((char) ('A' + rand.nextInt(26)));
		String symbols = "!\"$%^&*()_+{}:@~<>?,./;'#][=-\\|'";
		for (int i = 0; i < 1; i++)
			chars.add(symbols.charAt(rand.nextInt(symbols.length())));
		while (chars.size() < 8)
			chars.add((char) ('!' + rand.nextInt(93)));
		Collections.shuffle(chars);
		char[] arr = new char[chars.size()];
		for (int i = 0; i < chars.size(); i++)
			arr[i] = chars.get(i);
		String generatedPassword = String.valueOf(arr);
		return generatedPassword;
	}
}
