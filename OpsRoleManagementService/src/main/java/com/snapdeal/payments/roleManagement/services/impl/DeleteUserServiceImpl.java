package com.snapdeal.payments.roleManagement.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.request.DeleteUserRequest;
import com.snapdeal.payments.roleManagementModel.services.DeleteUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 
 * 8-Dec-2015
 */
@Service
@Slf4j
public class DeleteUserServiceImpl implements DeleteUserService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<DeleteUserRequest> requestValidator;

	@Override
	@Transactional
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void deleteUser(DeleteUserRequest user) {
		requestValidator.validate(user);
		if (!persistanceManager.isUserExist(user.getUserId()))
			throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		persistanceManager.deleteUser(user.getUserId());
	}
}
