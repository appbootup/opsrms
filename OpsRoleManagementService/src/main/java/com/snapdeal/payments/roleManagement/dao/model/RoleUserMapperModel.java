package com.snapdeal.payments.roleManagement.dao.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleUserMapperModel {
   private List<Integer> roleIds;
   private String userId;
}
