package com.snapdeal.payments.roleManagement.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.roleManagement.dao.model.RolePermissionMapperModel;
import com.snapdeal.payments.roleManagement.dao.model.RoleUserMapperModel;
import com.snapdeal.payments.roleManagementModel.request.Role;

@Component
public class UserRoleDaoImpl implements UserRoleDao {

   @Autowired
   private SqlSessionTemplate sqlSession;

   @Override
   public Integer addRole(String roleName) {
      sqlSession.insert("role.createRole", roleName);
      Integer roleId = sqlSession.selectOne("role.getRoleIdByName", roleName);
      return roleId;

   }

   @Override
   public void addUserRoleMapping(RoleUserMapperModel userRoleMap) {
      sqlSession.insert("role.saveRoleUserMapping", userRoleMap);

   }

   @Override
   public void savePermissionRoleMapping(RolePermissionMapperModel rolePermissionMap) {
      sqlSession.insert("role.saveRolePermissionMapping", rolePermissionMap);

   }

   @Override
   public List<Role> getAllRoles() {
      List<Role> roles = sqlSession.selectList("role.getAllRoles");
      return roles;
   }

   @Override
   public List<Role> getAllRolesByUser(String userName) {
      List<Role> roles = sqlSession.selectList("role.getAllRolesByUser", userName);
      return roles;
   }

   @Override
   public void deleteRole(String role) {
      // TODO Auto-generated method stub

   }

   @Override
   public void deleteUserRoleMappingByUser(String userId) {
      sqlSession.delete("role.deleteUserRoleMappingByUser", userId);

   }

   @Override
   public void deletePermissionRoleMappingByRoleId(Integer roleId) {
      sqlSession.delete("role.deletePermissionRoleMappingByRoleId", roleId);

   }

	@Override
	public List<Role> getRolesByRoleNames(List<String> listRoleNames) {
		List<Role> roles = sqlSession.selectList("role.getRolesByRoleNames", listRoleNames);
		return roles;
	}
	
	@Override
	   public List<String> getUsersByRoleCriteria(Set<String> userIds,String columnName,String searchString){
		   Map<String,String> requestObjectForRoles = new HashMap<String,String>();
		   //requestObject.put("userIds", userIds);
		   requestObjectForRoles.put("columnName", columnName);
		   requestObjectForRoles.put("searchString", searchString);
		   List<String> validRolesList = new ArrayList<String>();
		   validRolesList = sqlSession.selectList("role.getRolesByCriteria",requestObjectForRoles);
		   Map<String,Object> requestObjectForUserRoleMapping = new HashMap<String,Object>();
		   requestObjectForUserRoleMapping.put("userIds", userIds);
		   requestObjectForUserRoleMapping.put("roleIds", validRolesList);
		   List<String> userByRolesList = sqlSession.selectList("role.getUsersByRoleCriteria",requestObjectForUserRoleMapping);
		   return userByRolesList;
	   }

}
