package com.snapdeal.payments.roleManagement.services.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagementModel.commons.EntityConstants;
import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.interceptor.RequestHeaders;
import com.snapdeal.payments.roleManagementModel.interceptor.RoleMgmtRequestContext;
import com.snapdeal.payments.roleManagementModel.request.ChangePasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.CreateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.DeleteUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ForgotPasswordNotifyRequest;
import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllRolesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllUsersRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByIdRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.LogoutUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.roleManagementModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.UpdateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyCodeRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.roleManagementModel.response.CreateUserResponse;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllRolesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllUsersResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByIdResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByUserNameResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByCriteriaResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByIdsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByRoleResponse;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;
import com.snapdeal.payments.roleManagementModel.response.SendEmailAndSmsOnMerchantCreationResponse;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;
import com.snapdeal.payments.roleManagementModel.response.VerifyOtpForMobileVerificationResponse;
import com.snapdeal.payments.roleManagementModel.services.MerchantRoleMgmtService;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController extends AbstractController {

	@Autowired
	private RoleMgmtService roleMgmtService;

	@Autowired
	private MerchantRoleMgmtService merchantRoleMgmtService;

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)

	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.CREATE_USER, method = RequestMethod.POST)
	public ServiceResponse<CreateUserResponse> createUser(@RequestBody @Valid CreateUserRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if ((results.hasErrors() && null != results.getAllErrors())) {
			throw new ValidationException(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = RoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = RoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}
		ServiceResponse<CreateUserResponse> response = new ServiceResponse<CreateUserResponse>();
		CreateUserResponse createUserResponse = roleMgmtService.createUser(request);
		response.setResponse(createUserResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.CREATE_USER_WITH_LINK, method = RequestMethod.POST)
	public ServiceResponse<CreateUserResponse> createUserWithLink(@RequestBody @Valid CreateUserRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if (((results.hasErrors() && null != results.getAllErrors()))) {
			throw new ValidationException(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = RoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = RoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}
		if (request.isSendEmail()) {
			if (StringUtils.isEmpty(request.getLinkForSettingPassword()) || null == request.getEmailTemplate()
					|| StringUtils.isEmpty(request.getEmailTemplate().getKey())) {
				throw new ValidationException(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
			}
		}
		ServiceResponse<CreateUserResponse> response = new ServiceResponse<CreateUserResponse>();
		CreateUserResponse createUserResponse = roleMgmtService.createUser(request);
		response.setResponse(createUserResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.UPDATE_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> updateUser(@RequestBody UpdateUserRequest request) {
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = RoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = RoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.updateUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_USER_BY_ID, method = RequestMethod.GET)
	public ServiceResponse<GetUserByIdResponse> getUserById(@ModelAttribute GetUserByIdRequest request) {

		ServiceResponse<GetUserByIdResponse> response = new ServiceResponse<GetUserByIdResponse>();
		GetUserByIdResponse getUserByIdResponse = roleMgmtService.getUserById(request);
		response.setResponse(getUserByIdResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_USER_BY_USER_NAME, method = RequestMethod.GET)
	public ServiceResponse<GetUserByUserNameResponse> getUserByUserName(
			@ModelAttribute GetUserByUserNameRequest request) {

		ServiceResponse<GetUserByUserNameResponse> response = new ServiceResponse<GetUserByUserNameResponse>();
		GetUserByUserNameResponse getUserByUserNameResponse = roleMgmtService.getUserByUserName(request);
		response.setResponse(getUserByUserNameResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_USERS_BY_ROLE, method = RequestMethod.GET)
	public ServiceResponse<GetUsersByRoleResponse> getUsersByRole(@ModelAttribute GetUsersByRoleRequest request) {

		ServiceResponse<GetUsersByRoleResponse> response = new ServiceResponse<GetUsersByRoleResponse>();
		GetUsersByRoleResponse getUsersByRoleResponse = roleMgmtService.getUsersByRole(request);
		response.setResponse(getUsersByRoleResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.CHANGE_PASSWORD, method = RequestMethod.POST)
	public ServiceResponse<Void> changePassword(@RequestBody ChangePasswordRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.changePassword(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.FORGOT_PASSWORD, method = RequestMethod.POST)
	public ServiceResponse<Void> forgotPasswordNotify(@RequestBody ForgotPasswordNotifyRequest request) {

		throw new InternalServerException(ExceptionMessages.FORGOT_PASSWORD_INVALID);
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.LOGIN_USER, method = RequestMethod.POST)
	public ServiceResponse<LoginUserResponse> loginUser(@RequestBody LoginUserRequest request) {

		ServiceResponse<LoginUserResponse> response = new ServiceResponse<LoginUserResponse>();
		LoginUserResponse loginResponse = roleMgmtService.loginUser(request);
		GetAllRolesRequest getRolesRequest = new GetAllRolesRequest();
		GetAllRolesResponse getRolesResponse = new GetAllRolesResponse();
		getRolesResponse = roleMgmtService.getAllRoles(getRolesRequest);
		loginResponse.setAllRoles(getRolesResponse.getRoles());
		response.setResponse(loginResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.LOGOUT_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> logoutUser(@RequestBody @Valid LogoutUserRequest request, BindingResult results,
			HttpServletRequest httpRequest) {
		if (((results.hasErrors() && null != results.getAllErrors()))) {
			throw new ValidationException(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.logoutUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.SOCIAL_LOGIN_USER, method = RequestMethod.POST)
	public ServiceResponse<LoginUserResponse> socialLoginUser(@RequestBody SocialLoginUserRequest request) {

		ServiceResponse<LoginUserResponse> response = new ServiceResponse<LoginUserResponse>();
		response.setResponse(roleMgmtService.socialLoginUser(request));
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.DELETE_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> deleteUser(@RequestBody DeleteUserRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.deleteUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.VALIDATE_CODE, method = RequestMethod.POST)
	public ServiceResponse<Void> verifyCodeAndSetPassword(@RequestBody @Valid VerifyCodeRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if ((results.hasErrors() && null != results.getAllErrors())) {
			throw new ValidationException(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		ServiceResponse<Void> response = new ServiceResponse<Void>();
		roleMgmtService.verifyCodeAndSetPassword(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_ALL_USERS_BY_IDS, method = RequestMethod.POST)
	public ServiceResponse<GetUsersByIdsResponse> getUsersByIds(@RequestBody GetUsersByIdsRequest request) {

		ServiceResponse<GetUsersByIdsResponse> response = new ServiceResponse<GetUsersByIdsResponse>();
		GetUsersByIdsResponse getUsersByIdsResponse = roleMgmtService.getUsersByIds(request);
		response.setResponse(getUsersByIdsResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_ALL_USER, method = RequestMethod.GET)
	public ServiceResponse<GetAllUsersResponse> getAllUsers(@ModelAttribute GetAllUsersRequest request) {
		ServiceResponse<GetAllUsersResponse> response = new ServiceResponse<GetAllUsersResponse>();
		GetAllUsersResponse allUsers = roleMgmtService.getAllUsers(request);
		response.setResponse(allUsers);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.GET_USER_DETAILS_FROM_TOKEN, method = RequestMethod.POST)
	public ServiceResponse<GetUserByTokenResponse> getUserByToken(@RequestBody GetUserByTokenRequest request) {
		ServiceResponse<GetUserByTokenResponse> response = new ServiceResponse<GetUserByTokenResponse>();
		GetUserByTokenResponse getUserByTokenResponse = roleMgmtService.getUserByToken(request);
		response.setResponse(getUserByTokenResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@PreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = RestURIConstants.GET_USER_BY_CRITERIA, produces = RestURIConstants.APPLICATION_JSON, method = RequestMethod.POST)
	public ServiceResponse<GetUsersByCriteriaResponse> getMerchantsByCriteria(
			@RequestBody GetUsersByCriteriaRequest getSelectedUsersRequest) {
		ServiceResponse<GetUsersByCriteriaResponse> response = new ServiceResponse<GetUsersByCriteriaResponse>();
		GetUsersByCriteriaResponse getSelectedUsersResponse = roleMgmtService
				.getUsersByCriteria(getSelectedUsersRequest);
		response.setResponse(getSelectedUsersResponse);
		return response;
	}

	@ExceptionHandler(RoleMgmtException.class)
	public <T> ServiceResponse<T> handleException(RoleMgmtException exception) {
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException(exception.getMessage());
		roleException.setErrorCode(exception.getErrorCode());
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(Exception.class)
	public <T> ServiceResponse<T> handleException(Exception exception) {
		log.info("Exception" + exception.toString());
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(RuntimeException.class)
	public <T> ServiceResponse<T> handleException(RuntimeException exception) {
		log.info("Runtime Exception" + exception.toString());
		ServiceResponse<T> response = new ServiceResponse<T>();
		RoleMgmtException roleException = new RoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.SEND_EMAIL_AND_SMS_ON_MERCHANT_CREATION, method = RequestMethod.POST)
	public ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse> sendEmailAndSmsOnMerchantCreation(
			@RequestBody SendEmailAndSmsOnMerchantCreationRequest request) {
		ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse> response = new ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse>();
		SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreationResponse = merchantRoleMgmtService
				.sendEmailAndSmsOnMerchantCreation(request);
		response.setResponse(sendEmailAndSmsOnMerchantCreationResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.SEND_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> sendOtpForMobileVerification(@RequestBody GenerateOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.sendOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.SEND_DUMMY_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> sendDummyOtpForMobileVerification(
			@RequestBody GenerateOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.sendDummyOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.RESEND_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> resendOtpForMobileVerification(@RequestBody ResendOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.resendOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.VERIFY_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<VerifyOtpForMobileVerificationResponse> verifyOtpForMobileVerification(
			@RequestBody VerifyOtpForMobileVerificationRequest request) {
		ServiceResponse<VerifyOtpForMobileVerificationResponse> response = new ServiceResponse<VerifyOtpForMobileVerificationResponse>();
		VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerificationResponse = merchantRoleMgmtService
				.verifyOtpForMobileVerification(request);
		response.setResponse(verifyOtpForMobileVerificationResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.RESET_PASSWORD_TO_DUMMY_PASSWORD, method = RequestMethod.POST)
	public ServiceResponse<Boolean> resetPasswordToDummyPasswordForStaging(
			@RequestBody SetPasswordToDummyPasswordRequest request) {
		ServiceResponse<Boolean> response = new ServiceResponse<Boolean>();
		Boolean responseForResetPassword = merchantRoleMgmtService.resetPasswordToDummyPasswordForStaging(request);
		response.setResponse(responseForResetPassword);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = RestURIConstants.GET_USER_DETAILS_FROM_TOKEN_FOR_MERCHANT, method = RequestMethod.POST)
	public ServiceResponse<GetUserByTokenResponse> getUserByTokenForMerchant(
			@RequestBody GetUserByTokenRequest request) {
		ServiceResponse<GetUserByTokenResponse> response = new ServiceResponse<GetUserByTokenResponse>();
		GetUserByTokenResponse getUserByTokenResponse = roleMgmtService.getUserByTokenForMerchant(request);
		response.setResponse(getUserByTokenResponse);
		return response;
	}

}