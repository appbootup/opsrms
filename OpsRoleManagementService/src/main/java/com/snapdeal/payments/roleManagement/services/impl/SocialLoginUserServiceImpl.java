package com.snapdeal.payments.roleManagement.services.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.onecheck.social.entity.SocialInfo;
import com.snapdeal.onecheck.social.factory.SocialProviderFactory;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagement.handlers.TokenUtils;
import com.snapdeal.payments.roleManagementModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.roleManagementModel.dto.Token;
import com.snapdeal.payments.roleManagementModel.dto.TokenDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;
import com.snapdeal.payments.roleManagementModel.exceptions.ValidationException;
import com.snapdeal.payments.roleManagementModel.request.CreateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;
import com.snapdeal.payments.roleManagementModel.services.SocialLoginUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket
 *         24-Nov-2015
 */
@Service
@Slf4j
public class SocialLoginUserServiceImpl implements SocialLoginUserService {
   @Autowired
   private PersistanceManager persistanceManager;
   @Autowired
   private RoleMgmtService service;
   @Autowired
   private RequestParamValidatorForRole<SocialLoginUserRequest> requestValidator;

   @Autowired
   IDFactory idfactory;
   
   @Autowired
   TokenUtils tokenUtils;

   @Value("${expiry.token}")
   private int expiryDuration;
   private final String SOCIAL_USER_NAME = "SocialUser";
   private final String SOCIAL_USER_PASSWORD = IDFactory.forgotPasswordNotify();

	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public LoginUserResponse socialLoginUser(SocialLoginUserRequest socialLoginUserRequest) {
		requestValidator.validate(socialLoginUserRequest);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, expiryDuration);
		String emailId = socialLoginUserRequest.getEmailId();
		try {
			emailId = SocialProviderFactory
					.getEmailFromSocialInfo(new SocialInfo(socialLoginUserRequest.getSocialToken(), "google"));
		} catch (Exception e) {
			throw new InternalServerException(ExceptionMessages.SOCIAL_DETAILS_FETCH_EXCEPTION);
		}
		if (!emailId.equalsIgnoreCase(socialLoginUserRequest.getEmailId())) {
			throw new ValidationException(ExceptionMessages.SOCIAL_LOGIN_EXCEPTION);
		}

      User user = persistanceManager.getUserByUserName(emailId);
      if (user != null) {
         return createResponse(user);
      } else {
         CreateUserRequest request = new CreateUserRequest();
         request.setName(SOCIAL_USER_NAME);
         request.setEmail(emailId);
         request.setUserName(emailId);
         request.setPassword(SOCIAL_USER_PASSWORD);
         request.setSocialUser(true);
         request.setPasswordSet(false);
         User newUser = persistanceManager.getUserById(service.createUser(request).getUserId());
         return createResponse(newUser);
      }
   }

   private LoginUserResponse getResponseFromData(User user, Token token) {
	   LoginUserResponse response = new LoginUserResponse();
	   response.setToken(token);
	   response.setUser(user);
      return response;
   }

   private LoginUserResponse createResponse(User user) {
		Token token = new Token();
		TokenDTO tokenObj = persistanceManager.getTokenFromUsername(user.getUserName());
		GenerateTokenResponse response = tokenUtils.generateToken();
		token.setToken(response.getToken());
		tokenObj.setActive(true);
		tokenObj.setLoginTime(new Date());
		tokenObj.setUserName(user.getUserName());
		tokenObj.setTokenId(response.getTokenId());
		persistanceManager.insertOrUpdateToken(tokenObj);

		return getResponseFromData(user, token);
   }
}