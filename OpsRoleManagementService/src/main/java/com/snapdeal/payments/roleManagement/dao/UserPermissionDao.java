package com.snapdeal.payments.roleManagement.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.payments.roleManagement.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.roleManagementModel.request.Permission;

/**
 * @author shubham
 *         19-Oct-2015
 */
public interface UserPermissionDao {

   public List<Permission> getAllPermissions();

   public List<Permission> getAllPermissionsByUser(String userName);

   public List<Permission> getAllPermissionsByRoleId(Integer roleId);

   public Map<String, Integer> getAllUserPermissions(String userName);

   public void mapAllPermissionsWithUser(PermissionUserMapperModel userPermissionMapper);

   public void deleteUserPermissionMappingByUser(String userId);
   
   public List<String> getUsersByPermissionCriteria(Set<String> userIds,String columnName,String searchString);
}
