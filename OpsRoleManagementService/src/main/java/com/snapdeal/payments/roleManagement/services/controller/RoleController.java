package com.snapdeal.payments.roleManagement.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.request.CreateRoleRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllPermissionsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllRolesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.roleManagementModel.request.UpdateRoleRequest;
import com.snapdeal.payments.roleManagementModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllRolesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetRolesByRoleNamesResponse;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class RoleController extends AbstractController {

   @Autowired
   private RoleMgmtService roleMgmtService;
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @PreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.CREATE_ROLE, method = RequestMethod.POST)
   public ServiceResponse<Void> createRole(@RequestBody CreateRoleRequest request) {

      ServiceResponse<Void> response = new ServiceResponse<Void>();
      roleMgmtService.createRole(request);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @PreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.UPDATE_ROLE, method = RequestMethod.POST)
   public ServiceResponse<Void> updateRole(@RequestBody UpdateRoleRequest request) {

      ServiceResponse<Void> response = new ServiceResponse<Void>();
      roleMgmtService.updateRole(request);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @PreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_ROLES, method = RequestMethod.GET)
   public ServiceResponse<GetAllRolesResponse> getAllRoles(
            @ModelAttribute GetAllRolesRequest request) {

      ServiceResponse<GetAllRolesResponse> response = new ServiceResponse<GetAllRolesResponse>();
      GetAllRolesResponse allRoles = roleMgmtService.getAllRoles(request);
      response.setResponse(allRoles);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @PreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_PERMISSIONS, method = RequestMethod.GET)
   public ServiceResponse<GetAllPermissionsResponse> getAllPermissions(
            @ModelAttribute GetAllPermissionsRequest request) {

      ServiceResponse<GetAllPermissionsResponse> response = new ServiceResponse<GetAllPermissionsResponse>();
      GetAllPermissionsResponse allPermissions = roleMgmtService.getAllPermissions(request);
      response.setResponse(allPermissions);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @PreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_ROLES_BY_ROLENAMES, method = RequestMethod.POST)
   public ServiceResponse<GetRolesByRoleNamesResponse> getRolesByRoleNames(
            @RequestBody GetRolesByRoleNamesRequest request) {
      ServiceResponse<GetRolesByRoleNamesResponse> response = new ServiceResponse<GetRolesByRoleNamesResponse>();
      GetRolesByRoleNamesResponse roles = roleMgmtService.getRolesByRoleNames(request);
      response.setResponse(roles);
      return response;
   }

   @ExceptionHandler(RoleMgmtException.class)
   public <T> ServiceResponse<T> handleException(RoleMgmtException exception) {
      ServiceResponse<T> response = new ServiceResponse<T>();
      RoleMgmtException roleException = new RoleMgmtException(exception.getMessage());
      roleException.setErrorCode(exception.getErrorCode());
      response.setException(roleException);
      return response;
   }
   
   @ExceptionHandler(Exception.class)
   public <T> ServiceResponse<T> handleException(Exception exception) {
	      log.info("Exception" + exception.toString());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      RoleMgmtException roleException= new RoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}
   
   @ExceptionHandler(RuntimeException.class)
   public <T> ServiceResponse<T> handleException(RuntimeException exception) {
	      log.info("Runtime Exception" + exception.toString());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      RoleMgmtException roleException= new RoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}
}
