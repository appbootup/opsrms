package com.snapdeal.payments.roleManagement.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.snapdeal.payments.communicator.dto.CommunicationIntent;
import com.snapdeal.payments.communicator.exception.HttpTransportException;
import com.snapdeal.payments.communicator.exception.ServiceException;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.CommunicatorUtils;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.OTPUtils;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.commons.OTPState;
import com.snapdeal.payments.roleManagementModel.dto.GenerateOTPServiceDTO;
import com.snapdeal.payments.roleManagementModel.dto.UserOTPDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPServiceException;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.services.ResendOTPService;

/**
 * @author aniket 
 * 6-Jan-2016
 */
@Service
@Slf4j
public class ResendOTPServiceImpl implements ResendOTPService {
	
	final static String templateName = "resetpassword";
	
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<ResendOTPRequest> requestValidator;

	@Autowired
	IDFactory idfactory;
	
	@Autowired
	private OTPUtils otpUtils;
	
	@Autowired
	private CommunicatorUtils communicatorUtils;

	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GenerateOTPResponse resendOTP(ResendOTPRequest resendOTPRequest) {
		requestValidator.validate(resendOTPRequest);
		Optional<UserOTPDTO> currentOtpInfo = persistanceManager
				.getOTPFromId(resendOTPRequest.getOtpId());
		if (currentOtpInfo.isPresent()) {
			otpUtils.verifyFrozenAccount(currentOtpInfo.get().getUserId());
			OTPState currentOtpState = OTPUtils.getOtpState(currentOtpInfo);
			UserOTPDTO otp = getUserOTPEntity(currentOtpState,currentOtpInfo);
			CommunicationIntent intent = communicatorUtils.generateCommunicationIntent(currentOtpInfo.get().getEmail(), otp);
			
			try {
				log.info("Sending communication intent for userEmail: " +currentOtpInfo.get().getEmail() +" : " +intent.toString());
				communicatorUtils.sendCommunicationIntent(intent);
			}  catch (HttpTransportException ex){
				log.info("HttpTransportException while sending communication intent for userEmail: {} "
						+ "Exception: {}", currentOtpInfo.get().getEmail(), ex.getMessage());
			} catch (ServiceException ex) {
				log.info("ServiceException while sending communication intent for userEmail: {} "
						+ "Exception: {}", currentOtpInfo.get().getEmail(), ex.getMessage());
			} catch (Exception ex){
				log.info("Exception while sending communication intent for userEmail: {} "
						+ "Exception: {}", currentOtpInfo.get().getEmail(), ex.getMessage());
			}
			
			GenerateOTPResponse response = new GenerateOTPResponse(otp.getOtpId());
			return response;
		} else {
			throw new OTPServiceException(ExceptionMessages.INVALID_OTP_ID);
		}
	}
	
	public UserOTPDTO getUserOTPEntity(OTPState currentOtpState,Optional<UserOTPDTO> currentOtpInfo){
		switch (currentOtpState) {
		case DOES_NOT_EXIST:
			throw new OTPServiceException(
					ExceptionMessages.INVALID_OTP_ID);
		case NOT_ACTIVE:
			otpUtils.updateOTPState(currentOtpInfo.get());
			return otpUtils.generateNewOTPInfo(new GenerateOTPServiceDTO(currentOtpInfo.get().getUserId(), currentOtpInfo.get().getMobileNumber(), currentOtpInfo.get().getEmail()));
		case IN_EXPIRY:
			UserOTPDTO otp = otpUtils.updateResendAttempts(currentOtpInfo);
			otpUtils.incrementSendCount(otp);
			return otp;
		default:
			throw new OTPServiceException(ExceptionMessages.OTP_GENERIC_EXCEPTION);
		}
	}

}


