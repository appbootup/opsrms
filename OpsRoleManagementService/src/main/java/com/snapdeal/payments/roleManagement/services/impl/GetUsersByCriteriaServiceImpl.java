package com.snapdeal.payments.roleManagement.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ValidationException;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.dao.model.ColumnsConstants;
import com.snapdeal.payments.roleManagement.dao.model.UserCriteria;
import com.snapdeal.payments.roleManagement.dao.model.UserDaoModel;
import com.snapdeal.payments.roleManagement.dao.model.UserFieldColumnName;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.commons.EntityConstants;
import com.snapdeal.payments.roleManagementModel.dto.FieldFilter;
import com.snapdeal.payments.roleManagementModel.dto.RangeFilter;
import com.snapdeal.payments.roleManagementModel.dto.SortField;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByCriteriaResponse;
import com.snapdeal.payments.roleManagementModel.services.GetUsersByCriteriaService;

/**
 * @author aniket 
 * 26-Aug-2016
 */
@Service
@Slf4j
public class GetUsersByCriteriaServiceImpl implements GetUsersByCriteriaService {

	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<GetUsersByCriteriaRequest> requestValidator;
	
	private int totalCount = 0;
	
	private static boolean isTotalCountSet = false;

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	@Transactional
	public GetUsersByCriteriaResponse getUsersByCriteria(GetUsersByCriteriaRequest getSelectedUsersRequest) {
		isTotalCountSet = false;
		totalCount = 0;
		requestValidator.validate(getSelectedUsersRequest);
		boolean isSearchOnRole = false;
		boolean isSearchOnPermission = false;
		List<User> userListResponse = new ArrayList<User>();
		if(null != getSelectedUsersRequest.getSearchFields() && !getSelectedUsersRequest.getSearchFields().isEmpty())
		for(String searchFieldName : getSelectedUsersRequest.getSearchFields()){
			if(searchFieldName.equals(EntityConstants.PERMISSION_NAME)){
				isSearchOnPermission = true;
				getSelectedUsersRequest.getSearchFields().remove(searchFieldName);
				continue;
			}
			if(searchFieldName.equals(EntityConstants.ROLE_NAME)){
				isSearchOnRole = true;
				getSelectedUsersRequest.getSearchFields().remove(searchFieldName);
			}
			if(isSearchOnPermission && isSearchOnRole){
				break;
			}
		}
		Map<String,UserDaoModel> userMap = new HashMap<String,UserDaoModel>();
		UserCriteria userCriteria = getUserCriteriaFromRequest(getSelectedUsersRequest);
		userMap = persistanceManager.getSelectedUsers(userCriteria);
		
		//List<UserDaoModel> userList = persistanceManager.getSelectedUsers(getUserCriteriaFromRequest(getSelectedUsersRequest));
		Set<String> userIds = new HashSet<String>();
		List<String> userPermissionList = new ArrayList<String>();
		List<String>  userRoleList = new ArrayList<String>();
		userIds = userMap.keySet();
		if(null!= userMap && !userMap.isEmpty() && isSearchOnPermission){
			userPermissionList = persistanceManager.getUsersByPermissionCriteria(userIds,ColumnsConstants.PERMISSION_NAME,userCriteria.getSearchString());
		}
		if (null != userMap && !userMap.isEmpty() && isSearchOnRole){
			userRoleList = persistanceManager.getUsersByRoleCriteria(userIds,ColumnsConstants.ROLE_NAME,userCriteria.getSearchString());
		}
		userPermissionList.addAll(userRoleList);
		userListResponse = mapUserAndPermissionListToResponse(userMap,userPermissionList,userIds);
		GetUsersByCriteriaResponse response = new GetUsersByCriteriaResponse();
		response.setUserList(userListResponse);
		response.setTotalCount(totalCount);
		
		return response;
	}
	
	private UserCriteria getUserCriteriaFromRequest(GetUsersByCriteriaRequest request) {
		UserCriteria userCriteria = new UserCriteria();
		if (request.getPageNumber() > 1)
			userCriteria.setOffset(((request.getPageNumber() - 1) * request.getRecordsPerPage()));
		userCriteria.setRecordsPerPage(request.getRecordsPerPage());

		List<String> searchColumns = new ArrayList<String>();
		if (StringUtils.isNotBlank(request.getSearchString())) {
			if (request.getSearchFields() != null)
				for (String search : request.getSearchFields()) {
					searchColumns.add(getColumnNameForEntity(search));
					//searchColumns.add(UserFieldColumnName.fromValue(search).getColumnName());
				}
		}
		userCriteria.setSearchColumns(searchColumns);
		userCriteria.setSearchString("%" + StringUtils.lowerCase(request.getSearchString()) + "%");

		List<SortField> sortFields = new ArrayList<SortField>();
		if (request.getSortFields() != null && !request.getSortFields().isEmpty()) {
			for (SortField sort : request.getSortFields()) {
				sort.setFieldName(getColumnNameForEntity(sort.getFieldName()));
				//sort.setFieldName(UserFieldColumnName.fromValue(sort.getFieldName()).getColumnName());
				sortFields.add(sort);
			}
		}
		userCriteria.setSortColumns(sortFields);

		List<RangeFilter> rangeFields = new ArrayList<RangeFilter>();
		if (request.getRangeFilter() != null && !request.getRangeFilter().isEmpty()) {
			for (RangeFilter range : request.getRangeFilter()) {
				range.setFieldName(getColumnNameForEntity(range.getFieldName()));
				//range.setFieldName(UserFieldColumnName.fromValue(range.getFieldName()).getColumnName());
				rangeFields.add(range);
			}
		}
		userCriteria.setRangeFilter(rangeFields);

		List<FieldFilter> rowFilters = new ArrayList<FieldFilter>();
		if (request.getFilters() != null)
			for (FieldFilter filter : request.getFilters()) {
				FieldFilter rowFilter = new FieldFilter();
				rowFilter.setFieldName(getColumnNameForEntity(filter.getFieldName()));
				//rowFilter.setFieldName(UserFieldColumnName.fromValue(filter.getFieldName()).getColumnName());
				rowFilter.setValues(filter.getValues());
				rowFilters.add(rowFilter);
			}
		userCriteria.setFilters(rowFilters);
		return userCriteria;
	}
	
	private List<User> mapUserAndPermissionListToResponse(Map<String,UserDaoModel> userMap, List<String> userPermissionList,
															Set<String> mapKeys){
		List<User> responseList = new ArrayList<User>();
		if(null != userPermissionList && !userPermissionList.isEmpty()){
			for(String userId: userPermissionList){
				responseList.add(mapUserDaoModelToUser(userMap.get(userId)));
			}
		}
		else{
			for(String userId : mapKeys){
				responseList.add(mapUserDaoModelToUser(userMap.get(userId)));
			}
		}
		return responseList;
	}
	
	private User mapUserDaoModelToUser(UserDaoModel userDaoModel){
		User user = new User();
		if(!isTotalCountSet){
			totalCount = userDaoModel.getCount();
			isTotalCountSet = true;
		}
		user.setActive(userDaoModel.isActive());
		user.setEmail(userDaoModel.getEmail());
		user.setId(userDaoModel.getId());
		user.setMobile(userDaoModel.getMobile());
		user.setName(userDaoModel.getName());
		user.setUserName(userDaoModel.getUserName());
		user.setRoles(userDaoModel.getRoles());
		user.setPermissions(userDaoModel.getPermissions());
		return user;
	}
	
	private static String getColumnNameForEntity(String entity){
		String column = UserFieldColumnName.fromValue(entity).getColumnName();
		if(StringUtils.isBlank(column)){
			throw new ValidationException(ExceptionMessages.FIELD_NAME_IS_INVALID);
		}
		return column;
	}

}
