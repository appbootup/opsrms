package com.snapdeal.payments.roleManagement.services.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.handlers.AuthorizeHandlerImpl;
import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.exceptions.UnAuthrizedException;
import com.snapdeal.payments.roleManagementModel.request.AuthrizeUserRequest;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class AuthrizationController extends AbstractController {

   @Autowired
   private AuthorizeHandlerImpl authHandler;

   @PreAuthorize
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @RequestMapping(value = RestURIConstants.PREAUTHRIZE_TO_USER, method = RequestMethod.GET)
   public ServiceResponse<Void> getUsersByRole(@ModelAttribute AuthrizeUserRequest permissionRequest)
            throws UnAuthrizedException, ParseException {

      ServiceResponse<Void> response = new ServiceResponse<Void>();
      authHandler.handleAuthrization(permissionRequest.getPreAuthrizeString());
      return response;
   }

   @ExceptionHandler(RoleMgmtException.class)
   public <T> ServiceResponse<T> handleException(RoleMgmtException exception) {
      ServiceResponse<T> response = new ServiceResponse<T>();
      RoleMgmtException roleException = new RoleMgmtException(exception.getMessage());
      roleException.setErrorCode(exception.getErrorCode());
      response.setException(roleException);
      return response;
   }
   @ExceptionHandler(Exception.class)
   public <T> ServiceResponse<T> handleException(Exception exception) {
	      log.info("Exception" + exception.toString());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      RoleMgmtException roleException= new RoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}
   @ExceptionHandler(RuntimeException.class)
   public <T> ServiceResponse<T> handleException(RuntimeException exception) {
	      log.info("Runtime Exception" + exception.toString());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      RoleMgmtException roleException= new RoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}
}
