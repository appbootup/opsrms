/**
 * 26-Oct-2015
 *shubham
 */
package com.snapdeal.payments.roleManagement.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.GetUserByUserNameResponse;
import com.snapdeal.payments.roleManagementModel.services.GetUserByUserNameService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class GetUserByUserNameServiceImpl implements GetUserByUserNameService {

   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<GetUserByUserNameRequest> requestValidator;

   /*
    * (non-Javadoc)
    * 
    * @see
    * com.snapdeal.payments.roleManagement.services.GetUserByUserNameService#
    * getUserByUserName
    * (com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest
    * )
    */
	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUserByUserNameResponse getUserByUserName(GetUserByUserNameRequest userRequest) {
		requestValidator.validate(userRequest);
		User user = persistanceManager.getUserByUserName(userRequest.getUserName());
		if (user == null)
			throw new NoSuchUserException(ExceptionMessages.NO_SUCH_USER_EXCEPTION);

		return getResponseFromUser(user);
	}

   private GetUserByUserNameResponse getResponseFromUser(User data) {
      return new GetUserByUserNameResponse(data);
   }

}
