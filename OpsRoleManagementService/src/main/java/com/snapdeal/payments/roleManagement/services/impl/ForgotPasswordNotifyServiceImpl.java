package com.snapdeal.payments.roleManagement.services.impl;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagement.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.roleManagementModel.request.ForgotPasswordNotifyRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUserByUserNameResponse;
import com.snapdeal.payments.roleManagementModel.services.ForgotPasswordNotifyService;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

/**
 * @author aniket
 *         21-Nov-2015
 */
@Service
@Slf4j
public class ForgotPasswordNotifyServiceImpl implements ForgotPasswordNotifyService {

   final static String templateName = "resetpassword";
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<ForgotPasswordNotifyRequest> requestValidator;

   @Autowired
   private RoleMgmtService getUserByUserNameService;

   @Override
   @Transactional
   @Logged
   @Marked
   @Timed
   @ExceptionMetered
   public void forgotPasswordNotify(ForgotPasswordNotifyRequest request) {
      requestValidator.validate(request);
      String newPassword = IDFactory.forgotPasswordNotify();
      String hashedPassword = IDFactory.hashString(newPassword);
      GetUserByUserNameResponse user = getUserByUserNameService
               .getUserByUserName(getUserRequest(request.getUserName()));
      persistanceManager.resetPassword(request.getUserName(), hashedPassword, new Date());

      //notify.send(ChannelType.EMAIL, templateName, newPassword, user.getUser().getEmail());

   }

   private GetUserByUserNameRequest getUserRequest(String userName) {

      return new GetUserByUserNameRequest(userName);
   }

}