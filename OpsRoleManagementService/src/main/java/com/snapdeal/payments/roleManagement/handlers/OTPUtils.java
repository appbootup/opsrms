package com.snapdeal.payments.roleManagement.handlers;

import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.RandomStringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Optional;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagementModel.commons.OTPConstants;
import com.snapdeal.payments.roleManagementModel.commons.OTPState;
import com.snapdeal.payments.roleManagementModel.dto.FreezeAccountDTO;
import com.snapdeal.payments.roleManagementModel.dto.GenerateOTPServiceDTO;
import com.snapdeal.payments.roleManagementModel.dto.GenerateOtpServiceForMerchantDTO;
import com.snapdeal.payments.roleManagementModel.dto.UpdateInvalidAttemptsDTO;
import com.snapdeal.payments.roleManagementModel.dto.UpdateOTPStateDTO;
import com.snapdeal.payments.roleManagementModel.dto.UpdateResendAttemptsDTO;
import com.snapdeal.payments.roleManagementModel.dto.UserOTPDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPInvalidAttemptsExceededException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPResendAttemptsExceededException;
import com.snapdeal.payments.roleManagementModel.exceptions.OTPServiceException;
import com.snapdeal.payments.roleManagementModel.response.FrozenAccountResponse;

@Slf4j
@Component
public class OTPUtils {

	@Autowired
	private PersistanceManager persistanceManager;

	@Value("${otp.blockDurationInMins}")
	private Integer blockDurationInMins;

	@Value("${otp.reSendAttemptsLimit}")
	private Integer reSendAttemptsLimit;

	@Value("${otp.reSendAttemptsLimitForMerchant:3}")
	private Integer reSendAttemptsLimitForMerchant;

	@Value("${otp.expiryInMinsForSMS:2}")
	private Integer expiryDurationInMinsForSMS;

	@Value("${otp.expiryInMins}")
	private Integer expiryDurationInMins;

	@Getter
	@Value("${otp.invalidAttemptsLimit}")
	private Integer invalidAttemptsLimit;

	@Getter
	@Value("${otp.testEmail}")
	private String testEmail;

	@Getter
	@Value("${otp.sendToTestEmail}")
	private String sendToTestEmail;

	@Value("${otp.length}")
	private Integer length;

	@Value("${application.environment}")
	private String environment;

	public void verifyFrozenAccount(String userId) {
		Optional<FreezeAccountDTO> outdao = persistanceManager.getFreezedAccount(userId);
		FrozenAccountResponse frozenAccountResponse = calculateFrozenAccountResponse(outdao);
		if (frozenAccountResponse.isStatus() == true) {
			if (frozenAccountResponse.getRequestType().equalsIgnoreCase(OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS)) {
				throw new OTPServiceException(MessageFormat.format(ExceptionMessages.VERIFY_OTP_LIMIT_BREACHED,
						frozenAccountResponse.getRemainingMinutes() / 1));
			} else {
				throw new OTPServiceException(MessageFormat.format(ExceptionMessages.OTP_LIMIT_BREACHED,
						frozenAccountResponse.getRemainingMinutes() / 1));
			}
		}
	}

	public void verifyFrozenAccountForMerchant(String userId) {
		Optional<FreezeAccountDTO> outdao = persistanceManager.getFreezedAccount(userId);
		FrozenAccountResponse frozenAccountResponse = calculateFrozenAccountResponse(outdao);
		if (frozenAccountResponse.isStatus() == true) {
			if (frozenAccountResponse.getRequestType().equalsIgnoreCase(OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS)) {
				throw new OTPInvalidAttemptsExceededException(MessageFormat.format(
						ExceptionMessages.VERIFY_OTP_LIMIT_BREACHED, frozenAccountResponse.getRemainingMinutes() / 1));
			} else {
				throw new OTPResendAttemptsExceededException(MessageFormat.format(ExceptionMessages.OTP_LIMIT_BREACHED,
						frozenAccountResponse.getRemainingMinutes() / 1));
			}
		}
	}

	public static FrozenAccountResponse calculateFrozenAccountResponse(Optional<FreezeAccountDTO> outdao) {

		FrozenAccountResponse frozenAccountResponse = new FrozenAccountResponse();

		if (outdao.isPresent() == false) {
			frozenAccountResponse.setStatus(false);
			return frozenAccountResponse;
		} else {
			frozenAccountResponse.setRequestType(outdao.get().getFreezeReason());
			Date currentDate = new Date();
			if (currentDate.before((outdao.get()).getExpiryTime())) {
				long remainingTime = outdao.get().getExpiryTime().getTime() - Math.abs(System.currentTimeMillis());
				long remainingMintues = (remainingTime) / (1000 * 60) + 1;
				frozenAccountResponse.setRemainingMinutes(remainingMintues);
				frozenAccountResponse.setStatus(true);
				return frozenAccountResponse;
			}

		}
		frozenAccountResponse.setStatus(false);
		return frozenAccountResponse;
	}

	public void updateOTPState(UserOTPDTO otpinfo) {
		UpdateOTPStateDTO request = new UpdateOTPStateDTO();
		request.setOtpId(otpinfo.getOtpId());
		request.setOtpStateCurrent(OTPState.ACTIVE);
		request.setOtpStateExpected(OTPState.DELETED);
		persistanceManager.updateCurrentOTPStatus(request);
	}

	public UserOTPDTO generateNewOTPInfo(GenerateOTPServiceDTO generateOTPServiceRequest) {
		String otpString = getNewOtp();
		// String hashedOTP = IDFactory.hashString(unhashedOTP);
		UserOTPDTO otpInfo = new UserOTPDTO();
		String otpId = UUID.randomUUID().toString();
		otpInfo.setOtpId(otpId);
		otpInfo.setUserId(generateOTPServiceRequest.getUserId());
		otpInfo.setOtpState(OTPState.ACTIVE);
		otpInfo.setCreatedOn(new Date());
		otpInfo.setEmail(generateOTPServiceRequest.getEmailId());
		otpInfo.setExpiryTime(new DateTime().plusMinutes(expiryDurationInMins).toDate());
		if (null != generateOTPServiceRequest.getMobileNumber())
			otpInfo.setMobileNumber(generateOTPServiceRequest.getMobileNumber());
		else
			otpInfo.setMobileNumber("");
		otpInfo.setOtp(otpString);
		persistanceManager.saveOTP(otpInfo);
		// otpInfo.setOtp(unhashedOTP);
		return otpInfo;
	}

	public UserOTPDTO updateResendAttempts(Optional<UserOTPDTO> currentOtpInfo) {
		UserOTPDTO otp = currentOtpInfo.get();
		if (otp.getResendAttempts() >= reSendAttemptsLimit) {
			blockUser(otp, OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS);
		}
		return otp;
	}

	public UserOTPDTO updateResendAttemptsForMerchant(Optional<UserOTPDTO> currentOtpInfo) {
		UserOTPDTO otp = currentOtpInfo.get();
 		if (otp.getResendAttempts() >= reSendAttemptsLimitForMerchant) {
			blockUser(otp, OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS);
		}
		return otp;
	}

	public void updateInvalidAttempts(Optional<UserOTPDTO> currentOtpInfo) {
		UserOTPDTO otpInfo = currentOtpInfo.get();
		UpdateInvalidAttemptsDTO updateInvalidAttemptsRequest = new UpdateInvalidAttemptsDTO();
		updateInvalidAttemptsRequest.setOtpId(otpInfo.getOtpId());
		updateInvalidAttemptsRequest.setInvalidAttempts(otpInfo.getInvalidAttempts());
		updateInvalidAttemptsRequest.setReason(OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS);
		persistanceManager.incrementInvalidAttempts(updateInvalidAttemptsRequest);
	}

	public void updateBlockUser(UserOTPDTO otp) {
		FreezeAccountDTO frozenAccountInfo = new FreezeAccountDTO();
		frozenAccountInfo.setUserId(otp.getUserId());
		frozenAccountInfo.setExpiryTime(new DateTime().plusMinutes(blockDurationInMins).toDate());
		frozenAccountInfo.setFreezeReason(OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS);
		persistanceManager.updateFreezeUser(frozenAccountInfo);
	}

	public void blockUser(UserOTPDTO otp, String reason) {
		FreezeAccountDTO frozenAccountInfo = new FreezeAccountDTO();
		frozenAccountInfo.setExpiryTime(new DateTime().plusMinutes(blockDurationInMins).toDate());
		frozenAccountInfo.setFreezeReason(reason);
		frozenAccountInfo.setUserId(otp.getUserId());
		frozenAccountInfo.setIsdeleted("false");
		persistanceManager.freezeUser(frozenAccountInfo);
	}

	public void incrementSendCount(UserOTPDTO otpInfo) {
		UpdateResendAttemptsDTO updateResendAttemptsRequest = new UpdateResendAttemptsDTO();
		updateResendAttemptsRequest.setOtpId(otpInfo.getOtpId());
		updateResendAttemptsRequest.setMobileNumber(otpInfo.getMobileNumber());
		updateResendAttemptsRequest.setEmailId(otpInfo.getEmail());
		updateResendAttemptsRequest.setResendAttempts(otpInfo.getResendAttempts());
		persistanceManager.incrementResendAttempts(updateResendAttemptsRequest);
	}

	public static OTPState getOtpState(Optional<UserOTPDTO> currentOtpInfo) {
		if (currentOtpInfo.isPresent()) {
			UserOTPDTO otpInfo = currentOtpInfo.get();
			if (!otpInfo.getOtpState().equals(OTPState.ACTIVE))
				return OTPState.VERIFIED;
			Date currentDate = new Date();
			if (currentDate.before(otpInfo.getExpiryTime()))
				return OTPState.IN_EXPIRY;
			else
				return OTPState.NOT_ACTIVE;
		} else

			return OTPState.DOES_NOT_EXIST;
	}


	public UserOTPDTO generateOTPInfoForMobileVerification(
			GenerateOtpServiceForMerchantDTO generateOTPServiceForMerchantDtoRequest) {
		
		String otpString = getNewOtp();
		
		UserOTPDTO otpInfo = new UserOTPDTO();
		String otpId = UUID.randomUUID().toString();
		otpInfo.setOtpId(otpId);
		otpInfo.setUserId(generateOTPServiceForMerchantDtoRequest.getUserId());
		otpInfo.setOtpState(OTPState.ACTIVE);
		otpInfo.setCreatedOn(new Date());
		otpInfo.setExpiryTime(new DateTime().plusMinutes(expiryDurationInMinsForSMS).toDate());

		if (null != generateOTPServiceForMerchantDtoRequest.getMobileNumber())
			otpInfo.setMobileNumber(generateOTPServiceForMerchantDtoRequest.getMobileNumber());
		else
			otpInfo.setMobileNumber("");

		if (null != generateOTPServiceForMerchantDtoRequest.getEmailId())
			otpInfo.setEmail(generateOTPServiceForMerchantDtoRequest.getEmailId());
		else
			otpInfo.setEmail("");

		otpInfo.setOtp(otpString);
		persistanceManager.saveOTP(otpInfo);

		return otpInfo;
	}
	
	public UserOTPDTO generateDummyOTPInfoForMobileVerification(
			GenerateOtpServiceForMerchantDTO generateOTPServiceForMerchantDtoRequest) {
		
		String otpString = getNewOtpTemp();
		
		UserOTPDTO otpInfo = new UserOTPDTO();
		String otpId = UUID.randomUUID().toString();
		otpInfo.setOtpId(otpId);
		otpInfo.setUserId(generateOTPServiceForMerchantDtoRequest.getUserId());
		otpInfo.setOtpState(OTPState.ACTIVE);
		otpInfo.setCreatedOn(new Date());
		otpInfo.setExpiryTime(new DateTime().plusMinutes(expiryDurationInMinsForSMS).toDate());

		if (null != generateOTPServiceForMerchantDtoRequest.getMobileNumber())
			otpInfo.setMobileNumber(generateOTPServiceForMerchantDtoRequest.getMobileNumber());
		else
			otpInfo.setMobileNumber("");

		if (null != generateOTPServiceForMerchantDtoRequest.getEmailId())
			otpInfo.setEmail(generateOTPServiceForMerchantDtoRequest.getEmailId());
		else
			otpInfo.setEmail("");

		otpInfo.setOtp(otpString);
		persistanceManager.saveOTP(otpInfo);

		return otpInfo;
	}

	public String getNewOtpTemp() {
		return "1234";

	}

	public String getNewOtp() {
		return RandomStringUtils.randomNumeric(length);

	}
}