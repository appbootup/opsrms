package com.snapdeal.payments.roleManagement.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.communicator.dto.CommunicationIntent;
import com.snapdeal.payments.communicator.exception.HttpTransportException;
import com.snapdeal.payments.communicator.exception.ServiceException;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.roleManagement.dao.PersistanceManager;
import com.snapdeal.payments.roleManagement.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.roleManagement.dao.model.RoleUserMapperModel;
import com.snapdeal.payments.roleManagement.dao.model.UserDaoModel;
import com.snapdeal.payments.roleManagement.handlers.CipherServiceUtil;
import com.snapdeal.payments.roleManagement.handlers.CommunicatorUtils;
import com.snapdeal.payments.roleManagement.handlers.IDFactory;
import com.snapdeal.payments.roleManagementModel.dto.UserVerificationDTO;
import com.snapdeal.payments.roleManagementModel.exceptions.CipherException;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.roleManagementModel.exceptions.InternalServerException;
import com.snapdeal.payments.roleManagementModel.exceptions.UserAlreadyExistException;
import com.snapdeal.payments.roleManagementModel.request.CreateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByIdRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.roleManagementModel.request.User;
import com.snapdeal.payments.roleManagementModel.response.CreateUserResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByIdResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.roleManagementModel.services.CreateUserService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByIdService;
import com.snapdeal.payments.roleManagementModel.services.GetUserByTokenService;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

/**
 * @author shubham 26-Oct-2015
 */
@Service
@Slf4j
public class CreateUserServiceImpl implements CreateUserService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Value("${version.verificationCode}")
	private String verificationCodeVersion;

	@Value("${verificationCode.expiryInHours}")
	@Getter
	@Setter
	private Integer expiryDurationInHours;
	
	@Autowired
	private IDFactory idFactory;
	
	@Autowired
	CommunicatorUtils communicatorUtils;
	
	@Autowired
	@Qualifier("getUserByToken")
	GetUserByTokenService getUserByTokenService;
	
	@Autowired
	@Qualifier("getUserById")
	GetUserByIdService getUserByIdService;

	@Override
	@Transactional
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public CreateUserResponse createUser(CreateUserRequest user) {
		verifyAlreadyExist(user.getUserName());
		String userId = StringUtils
				.join(new String[] { IDFactory.getHourGranularity().toString(), IDFactory.getUUID() });
		String verificationCode = getVerificationCode();
		
		boolean sendEmail = user.isSendEmail();
		
		if (StringUtils.isEmpty(user.getPassword()))
			user.setPassword(IDFactory.forgotPasswordNotify());
		
		if (sendEmail)
			persistanceManager.saveUser(getUserDaoFromRequest(user, userId),
					getUserVerificationDTO(userId, verificationCode));
		else
			persistanceManager.saveUser(getUserDaoFromRequest(user, userId));
		
		if (null != user.getPermissionIds() && !user.getPermissionIds().isEmpty())
			persistanceManager.mapAllPermissionsWithUser(getPermissionMapperFromRequest(user, userId));

		if (null != user.getRoleIds() && !user.getRoleIds().isEmpty())
			persistanceManager.addUserRoleMapping(getRoleMapperFromRequest(user, userId));
		
		if (sendEmail) {
			String encryptedVerificationCode = "";
			try {
				encryptedVerificationCode = CipherServiceUtil.encrypt(verificationCode);
			} catch (CipherException e) {
				throw new InternalServerException(ExceptionMessages.SYSTEM_ERROR_EXCEPTION);
			}
			StringBuilder url = new StringBuilder(user.getLinkForSettingPassword());
			url.append(encryptedVerificationCode);
			
			CommunicationIntent intent = communicatorUtils.generateCommunicationIntent(user, url.toString());
			try {
				log.info("Sending communication intent for userEmail: " +user.getEmail() +" : " +intent.toString());
				communicatorUtils.sendCommunicationIntent(intent);
			} catch (HttpTransportException ex){
				log.info("HttpTransportException while sending communication intent for userEmail: {} "
						+ "Exception: {}", user.getEmail(), ex.getMessage());
			} catch (ServiceException ex) {
				log.info("ServiceException while sending communication intent for userEmail: {} "
						+ "Exception: {}", user.getEmail(), ex.getMessage());
			} catch (Exception ex){
				log.info("Exception while sending communication intent for userEmail: {} "
						+ "Exception: {}", user.getEmail(), ex.getMessage());
			}
		}
		
		try {
			String appName = user.getAppName();
			String token = user.getRequestToken();
			String userIdCallingtheAPI = null;
			String userNameCallingtheAPI = null;
			String NameCallingtheAPI = null;
			if(!StringUtils.isBlank(token)) {
			GetUserByTokenRequest req = new GetUserByTokenRequest();
			req.setRequestToken(token);
			GetUserByTokenResponse res = getUserByTokenService.getUserByToken(req);
			User user_calling_the_API = res.getUser();
			userIdCallingtheAPI = user_calling_the_API.getId();
			userNameCallingtheAPI = user_calling_the_API.getUserName();
			NameCallingtheAPI = user_calling_the_API.getName();
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String time = dateFormat.format(date);
			GetUserByIdRequest getUserByIdRequest = new GetUserByIdRequest();
			getUserByIdRequest.setUserId(userId);
			GetUserByIdResponse getUserByIdResponse = getUserByIdService.getUserById(getUserByIdRequest);
			User userInfoAfterUpdating = getUserByIdResponse.getUser();
		    log.info("USER_CREATED : CreateUserServiceImpl is called with appNameCallingtheAPI : {} ,userIdCallingtheAPI : {} ,userNameCallingtheAPI : {} ,NameCallingtheAPI : {} ,updatedOn : {} ,userIDGettingUpdated : {} ,"
				+ "userInfoAfterUpdating : {} ",appName,userIdCallingtheAPI,userNameCallingtheAPI,NameCallingtheAPI,time,userId,userInfoAfterUpdating);
			}
			catch (Exception e) {
			log.error("Error in logging data on user creation! ",e);
		}
		
		return new CreateUserResponse(userId);
	}

	private void verifyAlreadyExist(String userName) {
		if (persistanceManager.getUserByUserName(userName) != null)
			throw new UserAlreadyExistException(ExceptionMessages.USER_ALREADY_EXCEPTION);
	}

	private UserDaoModel getUserDaoFromRequest(CreateUserRequest request, String userId) {
		String hashedPassword = IDFactory.hashString(request.getPassword());

		return new UserDaoModel(userId, request.getName(), request.getEmail(), request.getMobile(), true,
				request.getUserName(), hashedPassword, request.isSocialUser(), request.isPasswordSet(),
				new DateTime().plusDays(idFactory.getPasswordExpiryInDays()).toDate(),null,null,0);
	}

	private PermissionUserMapperModel getPermissionMapperFromRequest(CreateUserRequest request, String userId) {
		return new PermissionUserMapperModel(request.getPermissionIds(), userId);
	}

	private RoleUserMapperModel getRoleMapperFromRequest(CreateUserRequest request, String userId) {
		return new RoleUserMapperModel(request.getRoleIds(), userId);
	}

	private UserVerificationDTO getUserVerificationDTO(String userId, String code) {
		return new UserVerificationDTO(code, userId,
				new DateTime().plusMinutes(getExpiryDurationInHours() * 60).toDate(), new Date());
	}

	private String getVerificationCode() {
		String uuid = UUID.randomUUID().toString();
		uuid = verificationCodeVersion + "#" + uuid;
		return uuid;
	}
}
