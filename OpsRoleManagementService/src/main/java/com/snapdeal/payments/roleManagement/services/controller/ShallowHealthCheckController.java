package com.snapdeal.payments.roleManagement.services.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ShallowHealthCheckController {
	public static final String RESPONSE200 = "HTTP 200 - OK";
	@RequestMapping(value = "/v1/health", method = {RequestMethod.HEAD,
			RequestMethod.GET})
	public String doShallowPing() {
		return RESPONSE200;
	}

}
