use role_mgmt;
insert into user(id,name,user_name,password,email,active,is_social_user,created_on,updated_on,is_password_set,password_expires_on) VALUES ('14628834kWduKRmiQEGnAhfiLNO8jM','selfserveadmin','selfserveadmin','e247eadabb033194a72a5ec62bc1f6965088c5af','vishal.agarwal@snapdeal.com',1,0,now(),now(),1,NULL);
insert into user_permission_mapping(permission_id,user_id) VALUES ((select id from permissions where permission_name='EDIT_MERCHANT_PROFILE'),(select id from user where user_name='selfserveadmin'));
insert into user_permission_mapping(permission_id,user_id) VALUES ((select id from permissions where permission_name='MANAGE_USER'),(select id from user where user_name='selfserveadmin'));
update user set password_expires_on = DATE_ADD(now(),INTERVAL 90 DAY);
