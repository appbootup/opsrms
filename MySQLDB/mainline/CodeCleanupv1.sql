use role_mgmt;
alter table user add is_password_set tinyint;
update user set password='8e7152d0eb52c340579f2d70a28eaf1a2c5ba1c5',is_password_set=1 where user_name='shubham.bansal@snapdeal.com';
update user set password='c6841d431eec0f629ad0f3bdfd5eae581d7d9dd9', is_password_set=1 where user_name='developer@tinyowl.co.in';
update user set password='6370da071510ff9a75284025d0f23f41ee55b5a4', is_password_set=1 where user_name='prantik.ghosh@shoppersstop.com';
update user set password='9955530ad3b311ae67e2442dd165e88e01982509', is_password_set=0 where name='SocialUser';

update permissions set display_name='View Merchant Profile' where permission_name='VIEW_MERCHANT_PROFILE';
update permissions set display_name='OPS Info Panel View Permission' where permission_name='OPS_USERPANEL_view';
update permissions set display_name='Manage User' where permission_name='MANAGE_USER';