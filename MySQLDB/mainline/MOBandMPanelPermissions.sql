use role_mgmt;
INSERT INTO permissions(app_name,permission_name,display_name) VALUES ('MOB','EDIT_MERCHANT_PROFILE','Edit Merchant Profile');
INSERT INTO permissions(app_name,permission_name,display_name) VALUES ('MOB','MERCHANT_SANDBOX_INT_KEY','View Sandbox Keys');
INSERT INTO permissions(app_name,permission_name,display_name) VALUES ('MOB','MERCHANT_PROD_INT_KEY','View Production Keys');