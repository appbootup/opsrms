insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_blackListUser","BlackList User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_bulk_blackListUser","Bulk BlackList User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_bulk_disableUser","Bulk Disable User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_bulk_enableUser","Bulk Enable User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_bulk_wallet_suspender","Bulk Wallet Suspender");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_bulk_whiteListUser","Bulk WhiteList User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_closeUserAccount","Close User Account");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_disableUser","Disable User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_enableUser","Enable User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_fraudManager","Fraud Manager");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_wallet_suspender","Wallet Suspender");
insert into permissions(app_name,permission_name,display_name) values("opspanel","actionpanel_whiteListUser","WhiteList User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","bulkpanel_bulk_User","Bulk User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","bulkpanel_bulk_superUser","Bulk Super User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","searchpanel_searchUser","Search User");
insert into permissions(app_name,permission_name,display_name) values("opspanel","searchpanel_searchTransaction","Search Transaction");
insert into permissions(app_name,permission_name,display_name) values("opspanel","opspanel_superUser","Super User");

