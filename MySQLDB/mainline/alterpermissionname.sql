use role_mgmt;
update permissions set permission_name="MANAGE_USER" where permission_name="SUPER_CREATE";
insert into permissions values(1002,"MOB","VIEW_MERCHANT_PROFILE",now(),now(),"View merchant profile");
update permissions set display_name="Manage user" WHERE permission_name="MANAGE_USER";
insert into role_permission_map values(1,1002);
insert into user_permission_mapping values(1002,1001);