use role_mgmt;
insert into permissions(app_name,permission_name,display_name) values("OPS_ACTIONPANEL","actionpanel_singleAction","Account Action");
insert into permissions(app_name,permission_name,display_name) values("OPS_ACTIONPANEL","actionpanel_bulkAction","Bulk Accounts Action");
insert into permissions(app_name,permission_name,display_name) values("OPSPANEL","ops_superuser","OPS SuperUser");
insert into permissions(app_name,permission_name,display_name) values("MPANEL","INITIATE_REFUND","Initiate Refund");
insert into permissions(app_name,permission_name,display_name) values("MPANEL","VIEW_STTL_REPORT","View Settlement Report");
insert into permissions(app_name,permission_name,display_name) values("MOB","VIEW_PROD_API","View Production API");
insert into permissions(app_name,permission_name,display_name) values("MOB","VIEW_SANDBOX","View Sandbox");
insert into permissions(app_name,permission_name,display_name) values("MOB","API_ACTIVATION","API Activation");