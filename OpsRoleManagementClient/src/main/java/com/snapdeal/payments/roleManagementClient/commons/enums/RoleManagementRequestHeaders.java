package com.snapdeal.payments.roleManagementClient.commons.enums;

public enum RoleManagementRequestHeaders {

   CONTENT_TYPE("content-type"), ACCEPT("accept"), CLIENT_ID("clientId"), CLIENT_IP_ADDRESS(
            "clientIpAddress"), USER_AGENT("user-Agent"), USER_MACHINE_IDENTIFIER(
            "userMachineIdentifier"), CLIENT_SDK_VERSION("client-version"), TIMESTAMP("timestamp"), REQUEST_URI(
            "Request-URI"), HASH("hash"), TOKEN("token"), APP_NAME("appName"), EMAIL_ID(
            "emailID"), MOBILE_NUMBER("mobileNumber"), HTTPMETHOD("httpmethod"), SERVER_IP_ADDRESS(
            "serverIpAddress"), APP_REQUEST_ID("apprequestid");

   private String description;

   private RoleManagementRequestHeaders(String description) {
      this.description = description;
   }

   @Override
   public String toString() {
      return description;
   }

}
