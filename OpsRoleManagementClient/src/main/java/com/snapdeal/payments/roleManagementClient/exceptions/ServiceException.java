package com.snapdeal.payments.roleManagementClient.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceException extends RuntimeException {

	private String errCode;
	   private String errMsg;

	   private static final long serialVersionUID = 1L;

	   public ServiceException(String errMsg, String errCode) {
	      super(errMsg);
	      this.errCode = errCode;
	      this.errMsg = errMsg;
	   }
	   
	   public ServiceException(){
		   
	   }
}