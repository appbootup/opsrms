package com.snapdeal.payments.roleManagementClient.utils;

import org.apache.mina.http.api.HttpMethod;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.snapdeal.payments.roleManagementClient.exceptions.HttpTransportException;
import com.snapdeal.payments.roleManagementClient.exceptions.ServiceException;
import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.request.AbstractRequest;
import com.snapdeal.payments.roleManagementModel.request.AuthrizeUserRequest;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;

/**
 * @author shubham
 *         17-Oct-2015
 * 
 *         Handle Authorization for apis
 */
@Aspect
@Component
public class PreAuthorizeHandler {

   protected <T extends AbstractRequest, R> R prepareResponse(T request,
            TypeReference<ServiceResponse<R>> typeReference, HttpMethod httpMthod, String uri)
            throws ServiceException, RoleMgmtException, HttpTransportException {

      final String completeURL = HttpUtil.getInstance().getCompleteUrl(uri);

      ServiceResponse<R> obj = (ServiceResponse<R>) HttpUtil.getInstance().processHttpRequest(
               completeURL, typeReference, request, httpMthod);
      RoleMgmtException exception = obj.getException();
      if (exception != null) {
         throw exception;
      }
      return obj.getResponse();
   }

   @Before("within(com.snapdeal..*) && @annotation(authorize)")
   public void handleAuthrization(PreAuthorize authorize) throws Exception {

      AuthrizeUserRequest request = new AuthrizeUserRequest();
      request.setPreAuthrizeString(authorize.value());

      prepareResponse(request, new TypeReference<ServiceResponse<Void>>() {
      }, HttpMethod.GET, RestURIConstants.PREAUTHRIZE_TO_USER);

   }
}
