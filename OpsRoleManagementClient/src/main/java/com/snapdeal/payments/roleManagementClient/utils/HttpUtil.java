package com.snapdeal.payments.roleManagementClient.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.mina.http.api.HttpMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snapdeal.payments.roleManagementClient.commons.enums.EnvironmentEnum;
import com.snapdeal.payments.roleManagementClient.commons.enums.RoleManagementRequestHeaders;
import com.snapdeal.payments.roleManagementClient.commons.enums.ValidResponseEnum;
import com.snapdeal.payments.roleManagementClient.exceptions.HttpTransportException;
import com.snapdeal.payments.roleManagementClient.exceptions.ServiceException;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.dto.ClientConfig;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.interceptor.RequestHeaders;
import com.snapdeal.payments.roleManagementModel.interceptor.RoleMgmtRequestContext;
import com.snapdeal.payments.roleManagementModel.request.AbstractRequest;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;

@Slf4j
public class HttpUtil {

   HttpSender httpSender = HttpSender.getInstance();

   private static HttpUtil instance = new HttpUtil();

   public static final int DEFAULT_ERROR_STATUS_CODE = 404;

   public static HttpUtil getInstance() {
      return instance;
   }

   private String version;
   private boolean isSecure;
   private EnvironmentEnum environment;

   public String getVersion() {
      return version;
   }

   private HttpUtil() {
      version = ClientConstants.CLIENT_SDK_VERSION;
      isSecure = ClientConstants.IS_SECURE_ENABLED;
      environment = ClientConstants.ENVIRONMENT;
   }

   public String getCompleteUrl(String relativeUrl) {
      return ClientDetails.getInstance().getUrl() + relativeUrl;
   }

   public <T extends AbstractRequest, R> R processHttpRequest(String completeUrl,
            TypeReference<ServiceResponse<R>> typeReference, T request, HttpMethod method)
            throws ServiceException, HttpTransportException {

      R response = null;
      int statusCode = DEFAULT_ERROR_STATUS_CODE;
      try {

         long startTime = System.currentTimeMillis();
         if (request.getClientConfig() == null) {
            request.setClientConfig(getDefaultClientConfig());
         }
         Map<String, String> header = createHeader(request, method);

         HttpResponse result;
         {
            final Map<String, String> parameters = RequestMapCreator.getMap(request);
            result = executeHttpMethod(completeUrl, parameters, header, method,
                     request.getClientConfig());
         }

         if (log.isDebugEnabled()) {
            log.debug("Time taken by Request Id " + request.getClientConfig().getAppRequestId()
                     + " for executing is :: " + (System.currentTimeMillis() - startTime) + " ms");
         }
         if (result != null && result.getStatusLine() != null) {
            statusCode = result.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(result.getEntity());
            ObjectMapper mapper = new ObjectMapper();

            for (ValidResponseEnum status : ValidResponseEnum.values()) {
               if (statusCode == status.getValue()) {
                  try {
                     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                     response = mapper.readValue(json, typeReference);
                  } catch (IOException e) {
                     throw new HttpTransportException(e.getMessage(),
                              ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode());
                  }
                  return response;
               }
            }
            handleInvalidResult(result, json, mapper);
         }
      } catch (HttpTransportException e) {
         throw e;
      } catch (ServiceException e) {
         throw e;
      } catch (Exception e) {
         log.error("Error in executing service", e);
         throw new HttpTransportException(e.getMessage(),
                  ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode());
      }
      return response;
   }

   private HttpResponse executeHttpMethod(String completeUrl, Map<String, String> parameters,
            Map<String, String> header, HttpMethod method, ClientConfig requestConfig)
            throws ServiceException {
      HttpResponse result = null;
      switch (method) {
         case GET:
            result = httpSender.executeGet(completeUrl, parameters, header, requestConfig);
            break;
         case PUT:
            result = httpSender.executePut(completeUrl, parameters, header, requestConfig);
            break;
         case POST:
            result = httpSender.executePost(completeUrl, parameters, header, requestConfig);
            break;
         case DELETE:
            result = httpSender.executeDelete(completeUrl, parameters, header, requestConfig);
            break;
         default:
            throw new UnsupportedOperationException("Server doesn't support http method: " + method);
      }
      return result;
   }

   private Map<String, String> createHeader(AbstractRequest request, HttpMethod method) {
      long timeStamp = System.currentTimeMillis();
      ClientConfig clientConfig = request.getClientConfig();

      Map<String, String> header = new HashMap<String, String>();
      header.put(RoleManagementRequestHeaders.CONTENT_TYPE.toString(),
               RestURIConstants.APPLICATION_JSON);
      header.put(RoleManagementRequestHeaders.ACCEPT.toString(), RestURIConstants.APPLICATION_JSON);
      header.put(RoleManagementRequestHeaders.CLIENT_SDK_VERSION.toString(), getVersion());
      header.put(RoleManagementRequestHeaders.TIMESTAMP.toString(), String.valueOf(timeStamp));
      header.put(RoleManagementRequestHeaders.APP_REQUEST_ID.toString(),
               clientConfig.getAppRequestId());
      if ((null != request.getRequestToken()) && (!request.getRequestToken().isEmpty())) {
         header.put(RoleManagementRequestHeaders.TOKEN.toString(), request.getRequestToken());
      } else {
         header.put(RoleManagementRequestHeaders.TOKEN.toString(),
                  RoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull());
      }
      if ((null != ClientDetails.getInstance().getAppName()) && (!ClientDetails.getInstance().getAppName().isEmpty())) {
          header.put(RoleManagementRequestHeaders.APP_NAME.toString(), ClientDetails.getInstance().getAppName());
       }
      else if ((null != request.getAppName()) && (!request.getAppName().isEmpty())) {
         header.put(RoleManagementRequestHeaders.APP_NAME.toString(), request.getAppName());
      } else {
         header.put(RoleManagementRequestHeaders.APP_NAME.toString(),
                  RoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull());
      }
      return header;
   }

   private void handleInvalidResult(HttpResponse result, String json, ObjectMapper mapper)
            throws ServiceException {

      ServiceException se = null;
      try {
         se = mapper.readValue(json, ServiceException.class);
         throw new ServiceException(se.getMessage(), se.getErrCode());
      } catch (ServiceException e) {
         throw e;
      } catch (Exception e) {
         throw new HttpTransportException(e.getMessage(),
                  ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode());
      }

   }

   public EnvironmentEnum getEnvironment() {
      return environment;
   }

   public boolean getIsSecure() {
      return isSecure;
   }

   private ClientConfig getDefaultClientConfig() throws ServiceException {

      ClientConfig config = new ClientConfig();
      config.setApiTimeOut(ClientDetails.getInstance().getApiTimeOut());
      return config;
   }

}