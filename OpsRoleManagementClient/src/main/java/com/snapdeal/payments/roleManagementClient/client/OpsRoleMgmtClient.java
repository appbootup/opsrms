package com.snapdeal.payments.roleManagementClient.client;

import org.apache.mina.http.api.HttpMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.snapdeal.payments.roleManagementClient.exceptions.HttpTransportException;
import com.snapdeal.payments.roleManagementClient.exceptions.ServiceException;
import com.snapdeal.payments.roleManagementClient.utils.ClientDetails;
import com.snapdeal.payments.roleManagementClient.utils.HttpUtil;
import com.snapdeal.payments.roleManagementModel.commons.RestURIConstants;
import com.snapdeal.payments.roleManagementModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.roleManagementModel.exceptions.NoSuchUserException;
import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;
import com.snapdeal.payments.roleManagementModel.request.AbstractRequest;
import com.snapdeal.payments.roleManagementModel.request.AuthrizeUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ChangePasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.CreateRoleRequest;
import com.snapdeal.payments.roleManagementModel.request.CreateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.DeleteUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ForgotPasswordNotifyRequest;
import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllPermissionsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllRolesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllUsersRequest;
import com.snapdeal.payments.roleManagementModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByIdRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.roleManagementModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.LogoutUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.UpdateRoleRequest;
import com.snapdeal.payments.roleManagementModel.request.UpdateUserRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyCodeRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.roleManagementModel.response.CreateUserResponse;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllRolesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllUsersResponse;
import com.snapdeal.payments.roleManagementModel.response.GetRolesByRoleNamesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByIdResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUserByUserNameResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByCriteriaResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByIdsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByRoleResponse;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;
import com.snapdeal.payments.roleManagementModel.response.SendEmailAndSmsOnMerchantCreationResponse;
import com.snapdeal.payments.roleManagementModel.response.ServiceResponse;
import com.snapdeal.payments.roleManagementModel.response.VerifyOtpForMobileVerificationResponse;
import com.snapdeal.payments.roleManagementModel.services.RoleMgmtService;

public class OpsRoleMgmtClient implements RoleMgmtService {
	protected <T extends AbstractRequest, R> R prepareResponse(T request,
			TypeReference<ServiceResponse<R>> typeReference, HttpMethod httpMthod, String uri)
					throws ServiceException, RoleMgmtException, HttpTransportException {

		final String completeURL = HttpUtil.getInstance().getCompleteUrl(uri);

		ServiceResponse<R> obj = (ServiceResponse<R>) HttpUtil.getInstance().processHttpRequest(completeURL,
				typeReference, request, httpMthod);
		if (obj.getException() != null) {
			RoleMgmtException exception = new RoleMgmtException(obj.getException().getMessage());
			exception.setErrorCode(obj.getException().getErrorCode());
			if (exception.getErrorCode().getErrorCode().equals(ExceptionErrorCode.No_SUCH_USER.getErrorCode())) {
				NoSuchUserException noSuchUserException = new NoSuchUserException(exception.getMessage());
				noSuchUserException.setErrorCode(exception.getErrorCode());
				throw noSuchUserException;
			}
			throw exception;
		}
		return obj.getResponse();
	}
	public OpsRoleMgmtClient() {
	}

	public OpsRoleMgmtClient(String uri, String port, int timeOut) throws Exception {
		ClientDetails.init(uri, port, timeOut);
	}

	@Override
	public void createRole(CreateRoleRequest createRole) throws ServiceException, HttpTransportException {
		prepareResponse(createRole, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.CREATE_ROLE);
	}

	@Override
	public void updateRole(UpdateRoleRequest updaterole) throws ServiceException, HttpTransportException {
		prepareResponse(updaterole, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.UPDATE_ROLE);
	}

	@Override
	public CreateUserResponse createUser(CreateUserRequest user) throws ServiceException, HttpTransportException {
		return prepareResponse(user, new TypeReference<ServiceResponse<CreateUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.CREATE_USER_WITH_LINK);
	}

	@Override
	public void updateUser(UpdateUserRequest user) throws ServiceException, HttpTransportException {
		prepareResponse(user, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.UPDATE_USER);

	}

	@Override
	public GetUserByIdResponse getUserById(GetUserByIdRequest userrequest)
			throws ServiceException, HttpTransportException {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByIdResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USER_BY_ID);
	}

	@Override
	public GetUserByUserNameResponse getUserByUserName(GetUserByUserNameRequest userRequest)
			throws ServiceException, HttpTransportException {
		return prepareResponse(userRequest, new TypeReference<ServiceResponse<GetUserByUserNameResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USER_BY_USER_NAME);
	}

	@Override
	public GetUsersByRoleResponse getUsersByRole(GetUsersByRoleRequest userrequest)
			throws ServiceException, HttpTransportException {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUsersByRoleResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USERS_BY_ROLE);
	}

	@Override
	public GetAllRolesResponse getAllRoles(GetAllRolesRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllRolesResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_ROLES);
	}

	@Override
	public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllPermissionsResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_PERMISSIONS);
	}

	@Override
	public void changePassword(ChangePasswordRequest changePassword) {
		prepareResponse(changePassword, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.CHANGE_PASSWORD);

	}

	@Override
	public void forgotPasswordNotify(ForgotPasswordNotifyRequest forgotPasswordNotify) {
		prepareResponse(forgotPasswordNotify, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.FORGOT_PASSWORD);

	}

	@Override
	public LoginUserResponse loginUser(LoginUserRequest loginUserRequest) {
		return prepareResponse(loginUserRequest, new TypeReference<ServiceResponse<LoginUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.LOGIN_USER);
	}

	@Override
	public void logoutUser(LogoutUserRequest logoutUserRequest) {
		prepareResponse(logoutUserRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.LOGOUT_USER);
	}

	@Override
	public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllUsersResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_USER);
	}

	@Override
	public LoginUserResponse socialLoginUser(SocialLoginUserRequest socialLoginUserRequest) {
		return prepareResponse(socialLoginUserRequest, new TypeReference<ServiceResponse<LoginUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SOCIAL_LOGIN_USER);
	}

	@Override
	public void deleteUser(DeleteUserRequest deleteUser) {
		prepareResponse(deleteUser, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.DELETE_USER);

	}

	@Override
	public GetUsersByIdsResponse getUsersByIds(GetUsersByIdsRequest getUsers) {
		return prepareResponse(getUsers, new TypeReference<ServiceResponse<GetUsersByIdsResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_ALL_USERS_BY_IDS);
	}

	@Override
	public GetRolesByRoleNamesResponse getRolesByRoleNames(GetRolesByRoleNamesRequest getRoles) {
		return prepareResponse(getRoles, new TypeReference<ServiceResponse<GetRolesByRoleNamesResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_ALL_ROLES_BY_ROLENAMES);
	}

	@Override
	public GetUserByTokenResponse getUserByToken(GetUserByTokenRequest userrequest) {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByTokenResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_DETAILS_FROM_TOKEN);
	}

	@Override
	public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOtpRequest) {
		return prepareResponse(generateOtpRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GENERATE_OTP);
	}

	@Override
	public GenerateOTPResponse resendOTP(ResendOTPRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.RESEND_OTP);
	}

	@Override
	public void verifyOTP(VerifyOTPRequest verifyOTPRequest) {
		prepareResponse(verifyOTPRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.VALIDATE_OTP);

	}

	public void authorizeUser(AuthrizeUserRequest request) {
		String permissions = request.getPreAuthrizeString();
		permissions = permissions.replaceAll("\\s+","");
		if (!permissions.isEmpty()) {
			final String BASE_PERMISSION_STRING = "(hasPermission('";
			String[] parts = permissions.split(",");
			int numberOfPermissions = parts.length;
			String preAuthrizeString = BASE_PERMISSION_STRING + parts[0].toString() + "')";
			while (numberOfPermissions > 1) {
				numberOfPermissions--;
				preAuthrizeString = preAuthrizeString + "and hasPermission('" + parts[numberOfPermissions] + "')";
			}
			preAuthrizeString = preAuthrizeString + ")";
			request.setPreAuthrizeString(preAuthrizeString);
		}
		prepareResponse(request, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.GET, RestURIConstants.PREAUTHRIZE_TO_USER);
	}

	@Override
	public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest) {
		prepareResponse(verifyCodeRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.VALIDATE_CODE);
	}
	
	@Override
	public GetUsersByCriteriaResponse getUsersByCriteria(GetUsersByCriteriaRequest getUsersByCriteriaRequest) {
		return prepareResponse(getUsersByCriteriaRequest, new TypeReference<ServiceResponse<GetUsersByCriteriaResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_BY_CRITERIA);
	}

	
	public SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreation(SendEmailAndSmsOnMerchantCreationRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_EMAIL_AND_SMS_ON_MERCHANT_CREATION);
	}
	
	
	public GenerateOTPResponse sendOtpForMobileVerification(GenerateOTPRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public GenerateOTPResponse sendDummyOtpForMobileVerification(GenerateOTPRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_DUMMY_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public GenerateOTPResponse resendOtpForMobileVerification(ResendOTPRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.RESEND_OTP_FOR_MOBILE_VERIFICATION);
	}
	

	public VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerification(VerifyOtpForMobileVerificationRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<VerifyOtpForMobileVerificationResponse>>() {
		}, HttpMethod.POST, RestURIConstants.VERIFY_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public Boolean resetPasswordToDummyPasswordForStaging(SetPasswordToDummyPasswordRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<Boolean>>() {
		}, HttpMethod.POST, RestURIConstants.RESET_PASSWORD_TO_DUMMY_PASSWORD);
	}
	
	
	public GetUserByTokenResponse getUserByTokenForMerchant(GetUserByTokenRequest userrequest) {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByTokenResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_DETAILS_FROM_TOKEN_FOR_MERCHANT);
	}

}