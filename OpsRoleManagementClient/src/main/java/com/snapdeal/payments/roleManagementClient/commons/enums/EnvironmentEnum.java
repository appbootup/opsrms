package com.snapdeal.payments.roleManagementClient.commons.enums;

public enum EnvironmentEnum {

   TESTING, DEVELOPMENT, PRODUCTION
}
