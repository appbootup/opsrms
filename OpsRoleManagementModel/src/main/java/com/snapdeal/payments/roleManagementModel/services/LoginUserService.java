package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;

/**
 * @author aniket
 *         23-Nov-2015
 */
public interface LoginUserService {

   public LoginUserResponse loginUser(LoginUserRequest loginUserRequest);
}
