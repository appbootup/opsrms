package com.snapdeal.payments.roleManagementModel.request;

import java.util.List;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class Role {
   private Integer id;
   private String name;
   private List<Permission> permissions;
}
