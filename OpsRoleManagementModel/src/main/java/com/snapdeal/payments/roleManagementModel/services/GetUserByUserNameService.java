package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUserByUserNameResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUserByUserNameService {

   public GetUserByUserNameResponse getUserByUserName(GetUserByUserNameRequest userRequest);
}
