package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUserByIdRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUserByIdResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUserByIdService {

   public GetUserByIdResponse getUserById(GetUserByIdRequest userrequest);
}
