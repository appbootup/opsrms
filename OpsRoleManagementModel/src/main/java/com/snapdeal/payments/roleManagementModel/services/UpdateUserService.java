package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.UpdateUserRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UpdateUserService {

   public void updateUser(UpdateUserRequest user);
}
