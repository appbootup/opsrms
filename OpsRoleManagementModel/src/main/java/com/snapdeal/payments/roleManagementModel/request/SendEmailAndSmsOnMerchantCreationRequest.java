package com.snapdeal.payments.roleManagementModel.request;

import org.hibernate.validator.constraints.NotBlank;
import lombok.Data;

@Data
public class SendEmailAndSmsOnMerchantCreationRequest extends AbstractRequest {

	
	private static final long serialVersionUID = 1L;
	@NotBlank
	private String userID;
	@NotBlank
	private String linkForSettingPassword;
	
	

}
