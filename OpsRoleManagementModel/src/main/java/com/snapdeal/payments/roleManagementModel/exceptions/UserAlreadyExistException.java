package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class UserAlreadyExistException extends ValidationException {

   private static final long serialVersionUID = -4212829204867344092L;

   public UserAlreadyExistException(RuntimeException e) {
      super(e);
   }

   public UserAlreadyExistException() {
      super();
   }

   public UserAlreadyExistException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.USER_ALREADY_EXIST);
   }
}
