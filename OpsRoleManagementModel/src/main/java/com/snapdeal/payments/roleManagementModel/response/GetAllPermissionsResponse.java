package com.snapdeal.payments.roleManagementModel.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.roleManagementModel.request.Permission;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllPermissionsResponse {

   private List<Permission> permissions;

}
