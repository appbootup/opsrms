package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class RoleNameAlreadyExistException extends ValidationException {

   private static final long serialVersionUID = -4212829204867344092L;

   public RoleNameAlreadyExistException(RuntimeException e) {
      super(e);
   }

   public RoleNameAlreadyExistException() {
      super();
   }

   public RoleNameAlreadyExistException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.ROLE_NAME_ALREADY_EXIST);
   }
}
