package com.snapdeal.payments.roleManagementModel.response;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.dto.Token;
import com.snapdeal.payments.roleManagementModel.request.Role;
import com.snapdeal.payments.roleManagementModel.request.User;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author aniket
 *         23-Nov-2015
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"token"})
public class LoginUserResponse {
	
	   private User user;
	   private List<Role> allRoles;
	   private Token token;
}
