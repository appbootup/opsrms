package com.snapdeal.payments.roleManagementModel.response;

import lombok.Data;

@Data
public class ResponseObject {
	private String error;
	private String data;
}
