package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author shubham
 *         21-Oct-2015
 */

@NoArgsConstructor
public class RoleMgmtException extends RuntimeException {

   private ExceptionErrorCode errorCode;
   private Class<? extends RoleMgmtException> exceptionCause;

   public RoleMgmtException withErrorCode(ExceptionErrorCode errorCode) {
      this.errorCode = errorCode;
      return this;

   }

   public RoleMgmtException(Throwable e) {
      super(e);
   }

   public RoleMgmtException(String message) {
      super(message);
   }

   public RoleMgmtException(String message, Throwable e) {
      super(message, e);
   }

   public ExceptionErrorCode getErrorCode() {
      return errorCode;
   }

   public void setErrorCode(ExceptionErrorCode errorCode) {
      this.errorCode = errorCode;
   }

   public Class<?> getExceptionCause() {
      return exceptionCause;
   }

   public void setExceptionCause(Class<? extends RoleMgmtException> exceptionCause) {
      this.exceptionCause = exceptionCause;
   }

   {
      this.setExceptionCause(this.getClass());
   }
   private static final long serialVersionUID = 1L;

}
