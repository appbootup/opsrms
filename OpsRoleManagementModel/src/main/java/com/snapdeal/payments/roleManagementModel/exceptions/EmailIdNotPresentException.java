package com.snapdeal.payments.roleManagementModel.exceptions;

public class EmailIdNotPresentException extends RoleMgmtException {
	
	private static final long serialVersionUID = 1L;

	public EmailIdNotPresentException(Exception e) {
	      super(e);
	   }

	   public EmailIdNotPresentException(Throwable e) {
	      super(e);
	   }

	   public EmailIdNotPresentException(String message) {
	      super(message);
	   }

	   {
	      this.setErrorCode(ExceptionErrorCode.NO_EMAIL_ID_PRESENT);
	   }

}
