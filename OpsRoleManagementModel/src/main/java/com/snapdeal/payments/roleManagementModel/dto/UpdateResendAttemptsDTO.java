package com.snapdeal.payments.roleManagementModel.dto;

import lombok.Data;

public @Data class UpdateResendAttemptsDTO {
	private String otpId;
	private String mobileNumber;
	private String emailId;
	private int resendAttempts;
}