package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on exceeding verification attempts
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPInvalidAttemptsExceededException extends RoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPInvalidAttemptsExceededException(Exception e) {
		super(e);
	}

	public OTPInvalidAttemptsExceededException(Throwable e) {
		super(e);
	}

	public OTPInvalidAttemptsExceededException(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.INVALID_OTP_ATTEMPTS_EXCEEDED);
	}

}
