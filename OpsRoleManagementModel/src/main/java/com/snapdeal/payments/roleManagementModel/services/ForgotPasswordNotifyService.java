package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.ForgotPasswordNotifyRequest;

/**
 * @author aniket
 *         21-Nov-2015
 */
public interface ForgotPasswordNotifyService {

   public void forgotPasswordNotify(ForgotPasswordNotifyRequest forgotPasswordNotify);
}
