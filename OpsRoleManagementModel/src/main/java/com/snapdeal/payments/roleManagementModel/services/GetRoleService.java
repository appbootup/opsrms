package com.snapdeal.payments.roleManagementModel.services;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.request.Role;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetRoleService {

   public List<Role> getAllRoles();

   public List<Role> getAllRolesByUser(String userName);
}
