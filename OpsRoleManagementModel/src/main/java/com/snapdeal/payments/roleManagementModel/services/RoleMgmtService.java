package com.snapdeal.payments.roleManagementModel.services;

/**
 * @author shubham 26-Oct-2015
 */
public interface RoleMgmtService extends CreateRoleService, UpdateRoleService, CreateUserService,
		 ChangePasswordService, ForgotPasswordNotifyService, LoginUserService,LogoutUserService,
         UpdateUserService, GetUserByIdService, GetUserByUserNameService, GetUsersByRoleService,
         UiDropDownsService, SocialLoginUserService, DeleteUserService, GetUsersByIdsService,
         GetRolesByRoleNamesService,GetUserByTokenService,GenerateOTPService,ResendOTPService,
         VerifyOTPService, VerifyCodeService,GetUsersByCriteriaService{

}
