package com.snapdeal.payments.roleManagementModel.response;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.request.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author aniket
 *         11-Dec-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetRolesByRoleNamesResponse {

   private List<Role> roles;

}
