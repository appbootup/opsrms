package com.snapdeal.payments.roleManagementModel.response;

import java.util.Date;

import com.snapdeal.payments.roleManagementModel.exceptions.RoleMgmtException;

import lombok.Data;

public @Data class ServiceResponse<T> {
	private T response;
	private RoleMgmtException exception;
	private Date serverTimeStamp;

	public void setResponse(T response) {
		this.response = response;
		this.serverTimeStamp = new Date();
	}
}
