package com.snapdeal.payments.roleManagementModel.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.snapdeal.payments.roleManagementModel.commons.PreAuthorize;

@Component
public class RoleMgmtRequestContextInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HandlerMethod handlerMethod = null;
		if(handler instanceof HandlerMethod){
			handlerMethod = (HandlerMethod) handler;
			PreAuthorize preAuthorize = handlerMethod.getMethod().getAnnotation(PreAuthorize.class);
			if (preAuthorize != null) {
				this.setRequestDetailMap(request);
				return true;
			}

		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		RoleMgmtRequestContext.resetRequestMap();
	}

	private void setRequestDetailMap(HttpServletRequest request) {
		RoleMgmtRequestContext.set(RequestHeaders.APP_NAME,
				request.getHeader(RequestHeaders.APP_NAME.getName()));
		RoleMgmtRequestContext.set(RequestHeaders.TOKEN,
				request.getHeader(RequestHeaders.TOKEN.getName()));

	}
}
