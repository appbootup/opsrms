package com.snapdeal.payments.roleManagementModel.response;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.request.Permission;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class GetRoleResponse {
   private String roleName;
   private List<Permission> permissions;
}
