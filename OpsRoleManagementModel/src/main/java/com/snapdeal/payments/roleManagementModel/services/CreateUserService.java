package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.CreateUserRequest;
import com.snapdeal.payments.roleManagementModel.response.CreateUserResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface CreateUserService {
   public CreateUserResponse createUser(CreateUserRequest user);
}
