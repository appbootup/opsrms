package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetAllPermissionsRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllRolesRequest;
import com.snapdeal.payments.roleManagementModel.request.GetAllUsersRequest;
import com.snapdeal.payments.roleManagementModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllRolesResponse;
import com.snapdeal.payments.roleManagementModel.response.GetAllUsersResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UiDropDownsService {

   public GetAllRolesResponse getAllRoles(GetAllRolesRequest request);

   public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request);

   public GetAllUsersResponse getAllUsers(GetAllUsersRequest request);

}
