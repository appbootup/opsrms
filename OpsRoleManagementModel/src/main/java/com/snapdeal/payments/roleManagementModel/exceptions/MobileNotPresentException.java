package com.snapdeal.payments.roleManagementModel.exceptions;


public class MobileNotPresentException extends RoleMgmtException {

	
	private static final long serialVersionUID = 1L;

	public MobileNotPresentException(Exception e) {
		super(e);
	}
    public MobileNotPresentException(Throwable e) {
		super(e);
	}
    public MobileNotPresentException(String message) {
		super(message);
	}
    {
	this.setErrorCode(ExceptionErrorCode.NO_EMAIL_AND_MOBILE_PRESENT);
	}

}
