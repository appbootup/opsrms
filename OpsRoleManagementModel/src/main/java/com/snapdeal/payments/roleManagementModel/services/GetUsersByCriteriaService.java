package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByCriteriaResponse;

/**
 * @author aniket
 *         26-Aug-2016
 */
public interface GetUsersByCriteriaService {

   public GetUsersByCriteriaResponse getUsersByCriteria(GetUsersByCriteriaRequest getUsersByCriteriaRequest);
}
