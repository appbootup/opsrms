package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * @author shubham
 *         17-Oct-2015
 */
public class UnAuthrizedException extends ValidationException {

   public UnAuthrizedException() {
      super();
   }

   public UnAuthrizedException(String message) {
      super(message);
   }
}
