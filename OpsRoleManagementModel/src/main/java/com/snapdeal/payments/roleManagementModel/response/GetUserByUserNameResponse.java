package com.snapdeal.payments.roleManagementModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.roleManagementModel.request.User;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByUserNameResponse {

   private User user;

}
