package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.LoginUserRequest;
import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.roleManagementModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.roleManagementModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;
import com.snapdeal.payments.roleManagementModel.response.SendEmailAndSmsOnMerchantCreationResponse;
import com.snapdeal.payments.roleManagementModel.response.VerifyOtpForMobileVerificationResponse;

/**  
 * @author roopali 11-Sep-2018
 * APIs written for TSM-583
 */

public interface MerchantRoleMgmtService {

	   SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreation(SendEmailAndSmsOnMerchantCreationRequest sendEmailOnMerchantCreationRequest);
	   GenerateOTPResponse sendOtpForMobileVerification(GenerateOTPRequest generateOtpRequest);
	   GenerateOTPResponse sendDummyOtpForMobileVerification(GenerateOTPRequest generateOtpRequest);
	   GenerateOTPResponse resendOtpForMobileVerification(ResendOTPRequest resendOTPRequest);
	   VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerification(VerifyOtpForMobileVerificationRequest verifyOtpForMobileVerificationRequest);
	   Boolean resetPasswordToDummyPasswordForStaging(SetPasswordToDummyPasswordRequest request);
}
