package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.LogoutUserRequest;

/**
 * @author aniket
 *         16-Feb-2016
 */
public interface LogoutUserService {

   public void logoutUser(LogoutUserRequest logoutUserRequest);
}
