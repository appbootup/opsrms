package com.snapdeal.payments.roleManagementModel.request;

import java.util.List;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
public class UpdateUserRequest extends AbstractRequest{
   @NotBlank
   private String userId;
   private String name;
   private String mobile;
   private boolean isActive = true;

   private List<Integer> roleIds;
   private List<Integer> permissionIds;

}
