package com.snapdeal.payments.roleManagementModel.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

/**
 * @author shubham 19-Oct-2015
 */
@Data
@ToString(exclude={"password"})
public class CreateUserRequest extends AbstractRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank
	private String name;
	@NotBlank
	private String email;
	private String mobile;

	@NotBlank
	private String userName;
	private String password;

	private String linkForSettingPassword;

	private boolean isSocialUser = false;
	@JsonIgnore
	private boolean isPasswordSet = true;
	private List<Integer> roleIds;
	private List<Integer> permissionIds;
	private EmailTemplate emailTemplate;
	
	public boolean isSendEmail(){
		if(null  != linkForSettingPassword || null != emailTemplate ){
			return true;
		}		
		return false;
	}
}
