package com.snapdeal.payments.roleManagementModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.roleManagementModel.request.User;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByIdResponse {
   private User user;

}
