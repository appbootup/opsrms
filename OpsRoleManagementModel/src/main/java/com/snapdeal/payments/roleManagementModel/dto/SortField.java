package com.snapdeal.payments.roleManagementModel.dto;

import lombok.Data;

@Data
public class SortField {
   String fieldName;
   boolean order;
}
