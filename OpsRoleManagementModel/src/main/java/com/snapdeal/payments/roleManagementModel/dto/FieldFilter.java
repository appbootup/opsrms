package com.snapdeal.payments.roleManagementModel.dto;

import java.util.List;

import lombok.Data;

@Data
public class FieldFilter {
   String fieldName;
   List<String> values;
}
