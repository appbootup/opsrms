package com.snapdeal.payments.roleManagementModel.response;

import java.util.List;

import com.snapdeal.payments.roleManagementModel.request.User;

import lombok.Data;

@Data
public class GetUsersByCriteriaResponse {
   private static final long serialVersionUID = -420223466518706L;
   int totalCount;
   private List<User> userList;
}
