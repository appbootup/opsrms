package com.snapdeal.payments.roleManagementModel.request;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author aniket
 *         21-Nov-2015
 */
@Data
public class ForgotPasswordNotifyRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 12453L;
   @NotBlank
   private String userName;

}
