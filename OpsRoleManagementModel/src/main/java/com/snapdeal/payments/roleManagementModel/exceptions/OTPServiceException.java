package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when there is something wrong with OTP
 * 
 * @author aniket
 *         20-Jan-2016
 *
 */

@NoArgsConstructor
public class OTPServiceException extends RoleMgmtException {

   public OTPServiceException(Exception e) {
      super(e);
   }

   public OTPServiceException(Throwable e) {
      super(e);
   }

   public OTPServiceException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.OTP_SERVICE);
   }

}
