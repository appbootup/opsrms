package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByIdsResponse;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface GetUsersByIdsService {

   public GetUsersByIdsResponse getUsersByIds(GetUsersByIdsRequest getUsers);

}
