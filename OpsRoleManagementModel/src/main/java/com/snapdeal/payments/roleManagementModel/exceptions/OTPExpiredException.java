package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when an Expired OTP is entered
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPExpiredException extends RoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPExpiredException(Exception e) {
		super(e);
	}

	public OTPExpiredException(Throwable e) {
		super(e);
	}

	public OTPExpiredException(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.OTP_EXPIRED);
	}

}

