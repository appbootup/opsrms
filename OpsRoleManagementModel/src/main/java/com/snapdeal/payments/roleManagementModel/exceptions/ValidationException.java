package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author shubham
 *         21-Oct-2015
 *
 */

@NoArgsConstructor
public class ValidationException extends RoleMgmtException {

   public ValidationException(Exception e) {
      super(e);
   }

   public ValidationException(Throwable e) {
      super(e);
   }

   public ValidationException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION);
   }

}
