package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.roleManagementModel.response.GetRolesByRoleNamesResponse;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface GetRolesByRoleNamesService {

   public GetRolesByRoleNamesResponse getRolesByRoleNames(GetRolesByRoleNamesRequest getRoles);

}
