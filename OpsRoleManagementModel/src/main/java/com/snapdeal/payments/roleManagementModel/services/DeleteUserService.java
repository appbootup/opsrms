package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.DeleteUserRequest;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface DeleteUserService {

   public void deleteUser(DeleteUserRequest deleteUser);
}
