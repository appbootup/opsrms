package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.CreateRoleRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface CreateRoleService {

   public void createRole(CreateRoleRequest createRole);
}
