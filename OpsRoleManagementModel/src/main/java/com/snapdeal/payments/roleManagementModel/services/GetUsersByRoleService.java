package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUsersByRoleResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUsersByRoleService {

   public GetUsersByRoleResponse getUsersByRole(GetUsersByRoleRequest userrequest);
}
