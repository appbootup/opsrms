package com.snapdeal.payments.roleManagementModel.request;

import lombok.Data;

@Data
public class RequestObject {
	public String request;
}
