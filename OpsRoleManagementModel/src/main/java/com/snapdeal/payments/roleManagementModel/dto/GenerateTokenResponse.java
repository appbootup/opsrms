package com.snapdeal.payments.roleManagementModel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author aniket
 * 	15-Feb-2016
 */
@NoArgsConstructor
@AllArgsConstructor
public @Data class GenerateTokenResponse {

	private String tokenId;
	private String token;
	
}
