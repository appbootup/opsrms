package com.snapdeal.payments.roleManagementModel.request;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class GetRoleRequest extends AbstractRequest{
   @NotNull
   private Integer id;
}
