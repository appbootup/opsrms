package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.UpdateRoleRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UpdateRoleService {

   public void updateRole(UpdateRoleRequest updaterole);
}
