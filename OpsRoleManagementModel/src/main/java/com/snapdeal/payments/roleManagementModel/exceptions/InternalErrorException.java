package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * 
 * Exception occurs when there is any internal exception Error code defines the
 * exact reason for the exception
 * 
 *
 * @author shubham
 *         21-Oct-2015
 *
 */

public class InternalErrorException extends RoleMgmtException {

   private static final long serialVersionUID = 8008343838743105044L;

   public InternalErrorException() {
   }

   public InternalErrorException(String message) {
      super(message);
   }

   public InternalErrorException(Throwable e) {
      super(e);
   }

   public InternalErrorException(Throwable e, String message) {
      super(message, e);
   }

   {
      this.setErrorCode(ExceptionErrorCode.INTERNAL_ERROR);
   }

}
