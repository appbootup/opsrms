package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GenerateOTPRequest;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;

/**
 * @author aniket
 *         5-Jan-2015
 */
public interface GenerateOTPService {

   public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOtpRequest);
}
