package com.snapdeal.payments.roleManagementModel.dto;

import com.snapdeal.payments.roleManagementModel.commons.OTPState;

import lombok.Data;

public @Data class UpdateOTPStateDTO {
	private String otpId;
	private OTPState otpStateCurrent;
	private OTPState otpStateExpected;
}