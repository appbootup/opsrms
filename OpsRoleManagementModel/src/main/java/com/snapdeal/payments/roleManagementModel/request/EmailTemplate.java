package com.snapdeal.payments.roleManagementModel.request;

/**
 * @author aniket
 *         8-Feb-2016
 */
public enum EmailTemplate {
	MERCHANT_PANEL_SETPASSWORD("merchant_panel_setPassword"), 
	OP_PANEL_SETPASSWORD("op_panel_setPassword"), 
	DEBUGGER_DASHBOARD_SETPASSWORD("debugger_dashboard_setPassword");

	 private final String emailTemplateName;

	   private EmailTemplate(String emailTemplateName) {
	      this.emailTemplateName = emailTemplateName;
	   }

	   public String getKey() {
	      return emailTemplateName;
	   }
}
