package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on exceeding OTP resend attempts
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPResendAttemptsExceededException extends RoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPResendAttemptsExceededException(Exception e) {
		super(e);
	}

	public OTPResendAttemptsExceededException(Throwable e) {
		super(e);
	}

	public OTPResendAttemptsExceededException(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.RESEND_OTP_ATTEMPTS_EXCEEDED);
	}

}
