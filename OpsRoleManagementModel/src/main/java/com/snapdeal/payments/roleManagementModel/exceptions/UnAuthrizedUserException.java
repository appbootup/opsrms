package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class UnAuthrizedUserException extends ValidationException {

   private static final long serialVersionUID = -4212829204867344092L;

   public UnAuthrizedUserException(RuntimeException e) {
      super(e);
   }

   public UnAuthrizedUserException() {
      super();
   }

   public UnAuthrizedUserException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.UNAUTHRIZED_USER);
   }
}
