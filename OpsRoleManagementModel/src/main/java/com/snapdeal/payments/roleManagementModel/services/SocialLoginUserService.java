package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.roleManagementModel.response.LoginUserResponse;

/**
 * @author aniket
 *         24-Nov-2015
 */
public interface SocialLoginUserService {

   public LoginUserResponse socialLoginUser(SocialLoginUserRequest socialLoginUserRequest);
}
