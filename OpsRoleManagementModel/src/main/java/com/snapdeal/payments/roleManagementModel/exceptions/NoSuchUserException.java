package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class NoSuchUserException extends ValidationException {

   private static final long serialVersionUID = -4212829204867344092L;

   public NoSuchUserException(RuntimeException e) {
      super(e);
   }

   public NoSuchUserException() {
      super();
   }

   public NoSuchUserException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.No_SUCH_USER);
   }
}
