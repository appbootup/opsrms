package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when an Expired OTP is entered
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPIdInvalidException extends RoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPIdInvalidException(Exception e) {
		super(e);
	}

	public OTPIdInvalidException(Throwable e) {
		super(e);
	}

	public OTPIdInvalidException(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.OTP_ID_INVALID);
	}

}
