package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.VerifyOTPRequest;

/**
 * @author aniket
 *         6-Jan-2015
 */
public interface VerifyOTPService {

   public void verifyOTP(VerifyOTPRequest verifyOTPRequest);
}
