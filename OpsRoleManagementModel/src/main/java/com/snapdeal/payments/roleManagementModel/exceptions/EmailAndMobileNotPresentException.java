package com.snapdeal.payments.roleManagementModel.exceptions;

public class EmailAndMobileNotPresentException extends RoleMgmtException{
	
	private static final long serialVersionUID = 1L;

	public EmailAndMobileNotPresentException(Exception e) {
		super(e);
	}
    public EmailAndMobileNotPresentException(Throwable e) {
		super(e);
	}
    public EmailAndMobileNotPresentException(String message) {
		super(message);
	}
    {
    	this.setErrorCode(ExceptionErrorCode.NO_EMAIL_AND_MOBILE_PRESENT);
	}

}
