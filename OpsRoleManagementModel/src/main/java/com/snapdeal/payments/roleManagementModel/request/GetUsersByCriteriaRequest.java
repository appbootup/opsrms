package com.snapdeal.payments.roleManagementModel.request;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.snapdeal.payments.roleManagementModel.dto.FieldFilter;
import com.snapdeal.payments.roleManagementModel.dto.RangeFilter;
import com.snapdeal.payments.roleManagementModel.dto.SortField;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of={"filters","rangeFilter","sortFields","searchFields","searchString","recordsPerPage","pageNumber"})
public class GetUsersByCriteriaRequest extends AbstractRequest {
   private static final long serialVersionUID = -4202298683466518706L;
   
   private List<FieldFilter> filters;
   private List<RangeFilter> rangeFilter;
   private List<SortField> sortFields;
   private List<String> searchFields;

   @Size(max=127)
   private String searchString;

   @Min(value = 1)
   @Max(value = 1000)
   @NotNull
   private Integer recordsPerPage;

   @Min(value = 1)
   @NotNull
   private Integer pageNumber;
}
