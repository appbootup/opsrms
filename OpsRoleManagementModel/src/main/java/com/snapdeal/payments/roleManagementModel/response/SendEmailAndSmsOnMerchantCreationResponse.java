package com.snapdeal.payments.roleManagementModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Roopali
 *         05-Sep-2018
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendEmailAndSmsOnMerchantCreationResponse {

private boolean sent_mail_status;
	  
}
