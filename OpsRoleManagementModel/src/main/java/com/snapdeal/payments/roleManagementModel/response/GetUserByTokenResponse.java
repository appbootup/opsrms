package com.snapdeal.payments.roleManagementModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.roleManagementModel.request.User;

/**
 * @author aniket
 *         29-Dec-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByTokenResponse {
   private User user;

}
