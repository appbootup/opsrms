package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when there is something wrong with OTP
 * 
 * @author aniket
 *         20-Jan-2016
 *
 */

@NoArgsConstructor
public class CipherException extends RoleMgmtException {

   public CipherException(Exception e) {
      super(e);
   }

   public CipherException(Throwable e) {
      super(e);
   }

   public CipherException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.CIPHER);
   }

}
