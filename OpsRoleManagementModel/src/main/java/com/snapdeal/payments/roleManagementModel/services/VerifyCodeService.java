package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.VerifyCodeRequest;

/**
 * @author aniket
 *         21-Jan-2015
 */
public interface VerifyCodeService {

   public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest);
}
