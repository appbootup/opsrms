package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.ChangePasswordRequest;

/**
 * @author aniket
 *         21-Nov-2015
 */
public interface ChangePasswordService {

   public void changePassword(ChangePasswordRequest changePassword);
}
