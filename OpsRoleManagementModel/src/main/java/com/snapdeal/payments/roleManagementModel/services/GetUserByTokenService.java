package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.roleManagementModel.response.GetUserByTokenResponse;


public interface GetUserByTokenService {

   public GetUserByTokenResponse getUserByToken(GetUserByTokenRequest userrequest);
   public GetUserByTokenResponse getUserByTokenForMerchant(GetUserByTokenRequest userrequest);
}
