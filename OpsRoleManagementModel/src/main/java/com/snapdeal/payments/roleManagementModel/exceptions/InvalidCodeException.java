package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author aniket
 *         21-Jan-2016
 *
 */

@NoArgsConstructor
public class InvalidCodeException extends RoleMgmtException {

   public InvalidCodeException(Exception e) {
      super(e);
   }

   public InvalidCodeException(Throwable e) {
      super(e);
   }

   public InvalidCodeException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.INVALID_CODE);
   }

}
