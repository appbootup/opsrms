package com.snapdeal.payments.roleManagementModel.services;

import com.snapdeal.payments.roleManagementModel.request.ResendOTPRequest;
import com.snapdeal.payments.roleManagementModel.response.GenerateOTPResponse;

/**
 * @author aniket
 *         6-Jan-2015
 */
public interface ResendOTPService {

   public GenerateOTPResponse resendOTP(ResendOTPRequest resendOTPRequest);
}
