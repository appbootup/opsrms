package com.snapdeal.payments.roleManagementModel.response;

import lombok.Data;

/**
 * @author aniket
 *         5-Jan-2016
 */
public @Data class FrozenAccountResponse {
	boolean status;
	String requestType;
	long remainingMinutes ;
}
