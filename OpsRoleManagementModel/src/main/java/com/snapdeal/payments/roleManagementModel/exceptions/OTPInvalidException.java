package com.snapdeal.payments.roleManagementModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on entering an invalid OTP
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPInvalidException extends RoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPInvalidException(Exception e) {
		super(e);
	}

	public OTPInvalidException(Throwable e) {
		super(e);
	}

	public OTPInvalidException(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.INVALID_OTP_ENTERED);
	}

}
