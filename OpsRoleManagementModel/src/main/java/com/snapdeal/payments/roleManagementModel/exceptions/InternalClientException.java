package com.snapdeal.payments.roleManagementModel.exceptions;

/**
 * 
 * Exception occurs when there is any internal exception in client code Error
 * code defines the exact reason for the exception e.g. while calling restAPI
 *
 * @author shubham
 *         21-Oct-2015
 *
 */

public class InternalClientException extends InternalErrorException {

   private static final long serialVersionUID = -4788605715033323479L;

   public InternalClientException() {
   }

   public InternalClientException(String message) {
      super(message);
   }

   public InternalClientException(String message, Throwable e) {
      super(e, message);
   }

   public InternalClientException(Throwable e) {
      super(e);
   }

   {
      setErrorCode(ExceptionErrorCode.CLIENT_INTERNAL);
   }

}
