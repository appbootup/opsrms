package com.snapdeal.payments.roleManagementModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

/**
 * @author aniket
 *         8-Dec-2015
 */
@Data
public class DeleteUserRequest extends AbstractRequest{
	private static final long serialVersionUID = 1212333453L;
   @NotBlank
   private String userId;
   
}
